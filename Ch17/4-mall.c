// 4-mall.c -- use the Queue interface
// rewrite of mall.c to simulate double booth having 2 queues
// problem loc 22605

// mall.c text location 21827
// compile with queue.c

#include <stdio.h>
#include <stdlib.h>   // for rand90 and srand()
#include <stdbool.h>
#include <time.h>     // for time()
#include "queue.h"    // ensure Item typedef is correct ****

#define MIN_PER_HR 60.0
#define NUM_QUEUES 2

bool newcustomer(double x);   // is there a new customer?
Item customertime(long when); // set customer parameters
void printDataItem(const char sym, const int dataInt, const int nrPrinted);

int main(void)
{
    Queue line[NUM_QUEUES];
    Item temp;                // new customer data
    int hours;                // hours of simulation
    int perhour;              // average # of arrivals per hour
    long cycle, cyclelimit;   // loop counter, limit
    long turnaways = 0;       // turned away by full queue
    long customers = 0;       // joined the queue
    long served[NUM_QUEUES];          // served during the simulation
    long total_served = 0;
    long sum_line = 0;        // cumulative line length
    int wait_time[NUM_QUEUES];  // time until sigmund is free
    double min_per_cust;      // average time between arrivals
    long line_wait = 0;       // cumulative time in line
    bool cust_queued;
    int queue_to_use;
    int i;                    // index variable
    int j;                    // index variable
    int numPrinted = 0;       // track whether newline needed
    long RQ_zero = 0;         // for testing
    long RQ_one = 0;          // for testing
    long errors = 0;           // for testing

    puts("\n");

    for (i = 0; i < NUM_QUEUES; i++) {
        InitializeQueue(&line[i]);
        wait_time[i] = 0;
        served[i] = 0;
    }

    srand((unsigned int) time(0));  // random initializing of rand()
    puts("Case Study: Sigmund Lander's Advice Booth");
    printf("Simulates %d booths with %d queues\n", NUM_QUEUES, NUM_QUEUES);
    puts("Enter the number of simulation hours:");
    scanf("%d", &hours);
    cyclelimit = MIN_PER_HR * hours;
    puts("Enter the average number of customers per hour:");
    scanf("%d", &perhour);
    min_per_cust = MIN_PER_HR / perhour;

    // main loop - one loop per min to number of hours input by user
    for (cycle = 0; cycle < cyclelimit; cycle++) {
        if (newcustomer(min_per_cust)) {
            cust_queued = false;
            // --------------------------------------------------------------
            queue_to_use++;   // take turns as a start; but, this is 
                              // overridden later if one queue is shorter - 
                              // a nod to the typical customer getting in
                              // the shortest line
            if (queue_to_use >= NUM_QUEUES) // go back to beginning if at end
                queue_to_use = 0;
            // --------------------------------------------------------------
//            if (!QueueIsFull(&line[0]))
//                printf("(0-ok) ");
//            if (!QueueIsFull(&line[1]))
//                printf("(1-ok) \n");
            for (i = 0; i < NUM_QUEUES; i++) {
                if (!QueueIsFull(&line[i])) {
                    cust_queued = true;
                    if (line[i].items < line[queue_to_use].items) {
                        queue_to_use = i;
                    }
                }
            }
            if (cust_queued) {
                customers++;
                temp = customertime(cycle);
                if (line[0].items == line[1].items) {
                    queue_to_use = rand() % NUM_QUEUES;
                    if (queue_to_use == 0) RQ_zero++;
                    else RQ_one++;
//                    printf("RQ_%d", queue_to_use);
                    if (queue_to_use > (NUM_QUEUES - 1)) errors++;
                }
//                printDataItem('+', queue_to_use, ++numPrinted);
                EnQueue(temp, &line[queue_to_use]);
            }
            else {
                turnaways++;
//                printf("*!* ");
            }
        }
        for (i = 0; i < NUM_QUEUES; i++) {
            if (wait_time[i] <= 0 && !QueueIsEmpty(&line[i])) {
                DeQueue (&temp, &line[i]);
//                printDataItem('-', i, ++numPrinted);
                wait_time[i] = temp.processtime;
                line_wait += cycle - temp.arrive;
                served[i]++;
                total_served++;
            }
            if (wait_time[i] > 0)
                wait_time[i]--;
            sum_line += QueueItemCount(&line[i]);
        }
//        printf("\nQ0=%d  Q1=%d ", line[0].items, line[1].items);
    }


    if (customers > 0) {
        printf("\ncustomers accepted: %ld\n", customers);
        for (i = 0; i < NUM_QUEUES; i++) {
            printf("  customers served [%d]: %ld\n", i, served[i]);
            total_served += served[i];
        }
        printf("         turnaways: %ld\n", turnaways);
        printf("average queue size: %.2f\n", 
                (double) sum_line / (cyclelimit * NUM_QUEUES));
        printf(" average wait time: %.2f minutes\n", 
                (double) line_wait / total_served);
        printf("Random use of Queue 0: %ld\n", RQ_zero);
        printf("Random use of Queue 1: %ld\n", RQ_one);
        printf("Errors: %ld\n", errors);
    }
    else
        puts("No customers!");
    for (i = 0; i < NUM_QUEUES; i++)
        EmptyTheQueue(&line[i]);

    puts("Bye!");
    puts("\n");

    return (0);
}

// x = average time, in minutes, between customers
// return value is true if customer shows up this minute
bool newcustomer(double x)
{
    if (rand() * x / RAND_MAX < 1)
        return (true);
    else
        return (false);
}

// when is the time at which the customer arrives
// function returns an Item structure with the arrival time
// set to when and the processing time set to a random value
// in the range 1 - 3
Item customertime(long when)
{
    Item cust;

    cust.processtime = rand() % 3 + 1;
    cust.arrive = when;

    return (cust);
}

void printDataItem(const char sym, const int dataInt, const int nrPrinted)
{
    printf("%c%d", sym, dataInt);
    if (!(nrPrinted % 20))
        printf("\n");
    else
        printf(", ");
}
