/* films2.c -- using a linked list of structures */
/* text loc 20891 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TSIZE 45

struct film {
    char title[TSIZE];
    int rating;
    struct film * next;
};

char * s_gets(char * st, int n);

int main(void)
{
    struct film * head = NULL;
    struct film * prev;
    struct film * current;
    
    char input[TSIZE];

    puts("\n");

/* Gather and store information */
    puts("Enter first movie title:");
    while (s_gets(input, TSIZE) != NULL && input[0] != '\0') {
        current = (struct film *)malloc(sizeof(struct film));
        if (head == NULL)
            head = current;
        else
            prev->next = current;
        current->next = NULL;
        strcpy(current->title, input);
        puts("Enter your rating <0-10>:");
        scanf("%d", &current->rating);
        while (getchar() != '\n')
            continue;
        puts("Enter next movie title (empty line to stop):");
        prev = current;
    }

/* Show list of movies */
    if (head == NULL)
        printf("No data entered");
    else
        printf("Here is the movie list:\n");
    current = head;
    while (current != NULL) {
        printf("Movie: %s  Rating: %d\n",
                current->title, current->rating);
        current = current->next;
    }

/* Program done, so free allocated memory */
    current = head;
    while (current != NULL) {
        free(current);
        current = current->next;
    }
    
    printf("Bye!\n");

    puts("\n");

    return (0);
}

char * s_gets(char * st, int n)
{
    char * ret_val;
    int i = 0;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        while (st[i] != '\n' && st[i] != '\0')
            i++;
        if (st[i] == '\n')
            st[i] = '\0';
        else                             // obviously we get here *only* if st[i] == '\0'
            while (getchar() != '\n')    // these two lines clean out the input buffer
                continue;                //    since we haven't yet found a '\n'
    }
    return ret_val;
}

