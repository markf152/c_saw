/* films1.c -- using an arrray of structures */
/* text loc 20760 */

#include <stdio.h>
#include <string.h>

#define TSIZE 45  // size of array to hold title
#define FMAX 5    // max number of film titles

struct film {
    char title[TSIZE];
    int rating;
};

char * s_gets(char * st, int n);

int main(void)
{
    struct film movies[FMAX];
    int i = 0;
    int j;

    puts("\n");

    puts("Enter first movie title:");
    while (i < FMAX && s_gets(movies[i].title, TSIZE) != NULL &&
           movies[i].title[0] != '\0') {
        puts("Enter your rating <0-10>:");
        scanf("%d", &movies[i++].rating);
        while (getchar() != '\n')
            continue;
        puts("Enter the next movie title (empty line to stop):");
    }
    if (i == 0)
        printf("No data entered.  ");
    else
        printf("Here is the movie list:\n");

    for (j = 0; j < i; j++)
        printf("Movie: %s  Rating: %d\n", movies[j].title,
               movies[j].rating);
    printf("Bye!\n");

    puts("\n");

    return (0);
}

char * s_gets(char * st, int n)
{
    char * ret_val;
    int i = 0;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        while (st[i] != '\n' && st[i] != '\0')
            i++;
        if (st[i] == '\n')
            st[i] = '\0';
        else                             // obviously we get here *only* if st[i] == '\0'
            while (getchar() != '\n')    // these two lines clean out the input buffer
                continue;                //    since we haven't yet found a '\n'
    }
    return ret_val;
}


