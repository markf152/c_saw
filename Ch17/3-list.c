/* 3-list.c -- functions supporting list operations */
/* problem location 22597 */

/* original list.c text location 21296 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "3-list.h"

/* interface functions   */

/* set the list to empty */
void InitializeList(List * plist)
{
    plist->items = 0;    
}

/* returns true if list is empty */
bool ListIsEmpty(const List *plist)
{
    if (plist->items == 0)
        return (true);
    else
        return (false);

    // better code: return(plist->items == 0);  !!!!
}

/* returns true if list is full */
bool ListIsFull(const List * plist)
{
//    Node * pt;
    bool full;

    if (plist->items == MAXSIZE)
        full = true;
    else
        full = false;

    return(full);

    // better code: return(plist->items == MAXSIZE);   !!!!
}

/* returns number of nodes */
unsigned int ListItemCount(const List * plist)
{
    return (plist->items);
}

// adds item to next available slot in list, if not full
bool AddItem (Item item, List * plist)
{
    if (ListIsFull(plist))
        return false;      /* quit function on failure */
    
    plist->entries[plist->items] = item;
    (plist->items)++;

    return (true);
}

/* visit each node and execute function pointed to by pfun */
void Traverse (const List * plist, void (* pfun)(Item item) )
{
    int x;

    for (x = 0; x < plist->items; x++)
        (*pfun)(plist->entries[x]);  /* apply function to item   */
}

/* free memory allocated by malloc() */
/* set list pointer to NULL          */
void EmptyTheList(List * plist)
{
    int x;

    for (x = 0; x < plist->items; x++) {
        strncpy(plist->entries[x].title, "\0", 1);
        plist->entries[x].rating = 0;
    }

    plist->items = 0;
}

