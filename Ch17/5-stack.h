/* 5-stack.h -- header file for Stack data type */

/* Part of solution to programming exercise nr 5 (Ch 17) */
/* also . . . answer to Chapter 17 Review Question 5.b */

#ifndef _5STACK_H_
#define _5STACK_H_

#include <stdbool.h>

#define MAXSTACK 500

typedef char Item;

typedef struct stack
{
    Item items[MAXSTACK]; // array to hold the items on the stack
    int top;              // location in array of 'top' of stack
} Stack;

/* function prototypes */

/* operation:      initialize list to empty                          */
/* preconditions:  ps points to a struct of type Stack               */
/* postconditions: the list is initialized to empty                  */
bool InitializeStack(Stack * ps);

/* operation:      determine if list is empty                        */
/* preconditions:  ps points to a struct of type Stack               */
/* postconditions: returns true if Stack is empty else false         */
bool StackIsEmpty(const Stack * ps);

/* operation:      determine if stack is full                        */
/* preconditions:  ps points to a struct of type Stack               */
/* postconditions: returns true if stack is full else false          */
bool StackIsFull(const Stack * ps);

// don't forget "else false" part
// don't forget const when appropriate

/* operation:      push Item on to top of stack                      */
/* preconditions:  ps points to a struct of type Stack               */
/* postconditions: returns true if successful in pushing item        */
/*                     on stack                                      */
bool pushStack(Stack * ps, Item item);

/* operation:      pop Item off the top of the stack                 */
/* preconditions:  ps points to a struct of type Stack               */
/* postconditions: returns true if successful in popping item        */
/*                 item from top of stack is copied to * pitem       */
/*                     off stack; false if stack is empty to start   */
bool popStack(Stack * ps, Item * pitem);

#endif

