/* 5-string_stack.c --                                             */
/* - user inputs a string                                          */
/* - program pushes characters onto a stack                        */
/* - program pops the characters off the stack and displays them,  */
/*   . . . resulting in the string displayed in reverse order      */

/* problem location 22607 */

/* compile with 5-stack.c */

#include <stdio.h>
#include <string.h>

#include "5-stack.h"

#define STR_LEN 256

char * s_gets(char * st, int n);

int main(void)
{
    Stack chrStack;
    char input_string[STR_LEN];
    char chr;
    int i = 0;  // index variable

    puts("\n");

    InitializeStack(&chrStack);

    puts("Enter a string (empty line to quit):");
    while (s_gets(input_string, STR_LEN) && input_string[0] != '\0') {
        i = 0;
        while (input_string[i] != '\0' && !StackIsFull(&chrStack))
            pushStack(&chrStack, input_string[i++]);
        while (!StackIsEmpty(&chrStack)) {
            popStack(&chrStack, &chr);
            putchar(chr);
        }
        puts("\n\nEnter a string (empty line to quit):");
    }

    puts("Bye!");

    puts("\n");

    return (0);
}

char * s_gets(char * st, int n)
{
    char * ret_val;
    int i = 0;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        while (st[i] != '\n' && st[i] != '\0')
            i++;
        if (st[i] == '\n')
            st[i] = '\0';
        else                             // obviously we get here *only* if st[i] == '\0'
            while (getchar() != '\n')    // these two lines clean out the input buffer
                continue;                //    since we haven't yet found a '\n'
    }
    return ret_val;
}
