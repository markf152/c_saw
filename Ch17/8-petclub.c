/* 8-petclub.c -- use a binary search tree                              */

/* This program is a modification of petclub.c: all pets with the same  */
/* name are stored in a list in the same node.                          */

/* When user chooses to find a pet, the program will ask for the name   */
/* and list all the pets, along with their types having that name       */

/* Programming Exercise location: 22610, book pg 859                    */

/* Original source code location: 22405, book pg 849                    */

/* try using a linked list - see films2.c, loc 20892, book pg 781       */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "8-tree.h"

char menu(void);
void addpet(Tree * pt);
void droppet(Tree * pt);
void showpets(const Tree * pt);
void findpet(const Tree * pt);
void printitem(Item item);
void uppercase(char * str);
char * s_gets(char * st, int n);

int main(void)
{
    Tree pets;
    char choice;
    int numPets = 0;

    puts("\n");

    InitializeTree(&pets);
    while ((choice = menu()) != 'q') {
//        printf("\n");
        switch (choice) {
            case 'a' : addpet(&pets);
                break;
            case 'l' : showpets(&pets);
                break;
            case 'f' : findpet(&pets);
                break;
            case 'n' : numPets = TreeItemCount(&pets);
                       printf("\nThere are %d pet", numPets);
                       if (numPets > 1)
                           printf("s");
                       printf(" in club\n");
                break;
            case 'd' : droppet(&pets);
                break;
            default  : puts("Switching error");
        }
    }
    DeleteAll(&pets);
    puts("Bye.");

    puts("\n");

    return (0);
}

char menu(void)
{
    int ch;

    puts("\nNerfville Pet Club Membership Program");
    puts("Enter the letter corresponding to your choice:");
    puts("a) add a pet          l) show list of pets");
    puts("n) number of pets     f) find pets");
    puts("d) delete a pet       q) quit");

    while ((ch = getchar()) != EOF) {
        while (getchar() != '\n')  /* discard rest of line */
            continue;
        ch = tolower(ch);
        if (strchr("alrfndq", ch) == NULL)
            puts("Please enter an a, l, f, n, d, or q:\n");
        else
            break;
    }
    if (ch == EOF)      /* make EOF cause program go quit */
        ch = 'q';

    return (ch);
}

void addpet(Tree * pt)
{
    Item temp;

    temp.head = (Petkind *) malloc(sizeof(Petkind));
    temp.head->next = NULL;

    if (TreeIsFull(pt))
        puts("No room in the club!");
    else {
        puts("Please enter name of pet:");
        s_gets(temp.petname, SLEN);
        puts("Please eneter pet kind:");
        s_gets(temp.head->kind, SLEN);
        uppercase(temp.petname);
        uppercase(temp.head->kind);
        AddItem(&temp, pt);
    }
}

void showpets(const Tree * pt)
{
    if (TreeIsEmpty(pt))
        puts("No entries!");
    else {
        printf("\n");
        Traverse(pt, printitem);
    }
}

void printitem(Item item)
{
    struct petkind * current = item.head;

    printf("Types of pets with name: %s\n", item.petname);

    while (current != NULL) {
        printf("%s\n", current->kind);
        current = current->next;
    }
    printf("\n");
}

void findpet(const Tree * pt)
{
    Item temp;

    if (TreeIsEmpty(pt)) {
        puts("No entries!");
        return;    /* quit function if tree is empty */
    }

    puts("Please enter name of pet you wish to find:");
    s_gets(temp.petname, SLEN);
    uppercase(temp.petname);

    if (InTree(&temp, pt)) {
            printf("The following pet types have a pet name %s in the club:\n",
                   temp.petname);
        SingleItemFunc (pt, temp, printitem);
    }
    else
        printf("No pet named %s is a member of the club.\n",
               temp.petname);
}

void droppet(Tree * pt)
{
    Item temp;

    temp.head = (Petkind *) malloc(sizeof(Petkind));
    temp.head->next = NULL;

    if (TreeIsEmpty(pt)) {
        puts("No entries!");
        return;    /* quit function if tree is empty */
    }

    puts("Please enter name of pet you wish to delete:");
    s_gets(temp.petname, SLEN);
    puts("Please enter pet kind:");
    s_gets(temp.head->kind, SLEN);
    uppercase(temp.petname);
    uppercase(temp.head->kind);
    printf("%s the %s", temp.petname, temp.head->kind);
    if (DeleteItem(&temp, pt))
        printf(" is dropped from the club.\n");
    else
        printf(" is not a member. Can't delete.\n");
}

void uppercase(char * str)
{
    while (*str) {
        *str = toupper(*str);
        str++;
    }
}

char * s_gets(char * st, int n)
{
    char * ret_val;
    int i = 0;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        while (st[i] != '\n' && st[i] != '\0')
            i++;
        if (st[i] == '\n')
            st[i] = '\0';
        else              // obviously we get here *only* if st[i] == '\0'
            while (getchar() != '\n') // these two lines clean out the input buffer
                continue;             //    since we haven't yet found a '\n'
    }
    return ret_val;
}

