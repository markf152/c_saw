/* 6-bin_int_search.c -- stub to test function: */
/* function takes 3 arguments . . .             */
/*     name of array of sorted integers         */
/*     number of elements in that array         */
/*     integer to seek                          */
/* function returns value 1 if integer is in    */
/*  array, and value 0 if it is not             */
/* function should use binary search technique  */

#include <stdio.h>
#include <stdbool.h>

int findIntBin (const int * intArray, int numEls, int seekInt);

int main(void)
{
    int numArr[50] = { 2,  3,  5,  6,  7,  9, 11, 13, 16, 17, 
                      19, 20, 22, 23, 25, 27, 30, 33, 34, 36,
                      37, 38, 40, 41, 42, 43, 47, 49, 50, 51,
                      55, 56, 57, 58, 60, 61, 64, 66, 67, 68,
                      69, 72, 74, 75, 77, 79, 82, 83, 85, 99 };
    int seekInt;
    int numElements;

    puts("\n");

    puts("Enter a number between 0 and 99 to see if it is in the array.");
    scanf("%d", &seekInt);
    if (seekInt > 99) {
        seekInt = 99;
        puts("\n** Number entered is greater than 99; changing it to 99 **");
    }
    if (seekInt < 0) {
        seekInt = 0;
        puts("\n** Number entered is less than 0; changing it to 0 **");

    }
    
    numElements = sizeof(numArr) / sizeof(numArr[0]);
    findIntBin(numArr, numElements, seekInt);

    puts("\n");

    return (0);
}

int findIntBin (const int * intArray, int numEls, int seekInt)
{
    int searchUpper = 0;
    int searchLower = 0;
    int middle = 0;
    bool done = false;
    int i;  // index variable

    searchLower = 0;
    searchUpper = (numEls - 1);

// This section neatly prints the array
    printf("\n");
    for (i = 0; i < numEls; i++) {
        if (intArray[i] < 10)
            printf(" ");
        printf("%d", intArray[i]);
        if ((i + 1) % 10)
            printf(", ");
        else
            printf("\n");
    }
    printf("\n");

// This section searches for the user's number in the array
    while (searchLower <= searchUpper) {
        printf("Lower = %d  Upper = %d\n", searchLower, searchUpper);
        middle = (searchLower + searchUpper) / 2;
        if (seekInt < intArray[middle])
            searchUpper = (middle - 1);
        else if (seekInt > intArray[middle])
            searchLower = (middle + 1);
        else {
            printf("%d found in location %d\n", seekInt, middle);
            return (0);
        }
    }
    printf("%d not found in array\n", seekInt);
    return (1);
}


