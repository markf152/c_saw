/* 5-stack.c -- implementation for Stack data type */

#include <stdio.h>
#include <stdlib.h>
#include "5-stack.h"

bool InitializeStack(Stack * ps)
{
    ps->top = 0;
}

bool StackIsEmpty(const Stack * ps)
{
    return (ps->top == 0);
}

bool StackIsFull(const Stack * ps)
{
    return (ps->top == MAXSTACK);
}

bool pushStack(Stack * ps, Item item)
{
    if (StackIsFull(ps))
        return (false);
    else {
        ps->items[ps->top++] = item;
        return (true);
    }
}

bool popStack(Stack * ps, Item * pitem)
{
    if (StackIsEmpty(ps))
        return (false);
    else {
        ps->top--;
        *pitem = ps->items[ps->top];
        return (true);
    }

}



