/* 7-word_search.c -- program opens a text file                */
/* - records how many times each word occurs in the file       */
/* - uses binary search tree to store each word and how many   */
/*        times it occurs in the file                          */
/* - offers 3 menu choices:                                    */
/*   (1) list all words with number of occurences              */ 
/*   (2) report number of occurences of a user-entered         */
/*        word in the file                                     */
/*   (3) quit                                                  */
/* exercise location 22612                                     */

/* *********************************************************** */
/*         -------- compile with 7-tree.c --------             */
/* *********************************************************** */

    // make file name a command line parameter
    // read in text file, one word at a time, to a linked list 
    //   - File I/O is ch 13
    //   - some example code is at loc 15796
    // counting words as we go -- need to search list as each word is encountered
    // (1) traverse binary search tree to list all words
    // (2) search binary tree to find user-entered word 

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include "7-tree.h"

void word_to_tree(const char * word, Tree * list_of_words);
char * s_gets(char * st, int n);
char get_choice(void);
void list_all_words(const Tree * word_list);
void one_word_report(const Tree * word_list);
void print_word_info(Item item);

int main(int argc, char * argv[])
{
    FILE * fp;                // file pointer to open text file
    Tree word_list;
    int ch;                   // storage for each character read in from file
    char input_string[SLEN];  // storage to build each word as it is read in 
                              //    from file
    int char_num = 0;         // counts characters read in from text file for
                              //    current word
    long words_in = 0;        // counts words read in from text file
    bool word_end = false;    // set to true when non "alpha" character is read
                              //     in from text file marking the end of a word
    char menu_choice = 'x';

    puts("\n");

    if (argc != 2) {
        printf("Usage: %s filename\n", argv[0]);
        exit (EXIT_FAILURE);
    }

    if ((fp = fopen(argv[1], "r")) == NULL) {
        printf("Can't open %s\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    printf("Opened file\n");

    InitializeTree(&word_list);

    while ((ch = getc(fp)) != EOF) {
//        printf("got here; ");
//        putc(ch, stdout);   // same as putchar(ch);
        if (isalpha(ch)) {
            input_string[char_num] = (char)ch;
            char_num++;
//            printf("ac; ");
        }
        else {
//            printf("got to else; ");
            if (char_num > 0) {
                words_in++;
                input_string[char_num] = '\0';
                word_end = true;
            }
        }
        if (word_end) {
//            printf("added word %ld; ", words_in);
            word_to_tree(input_string, &word_list);
            char_num = 0;
            printf("%s ", input_string);
            if (!(words_in % 10))
                printf(" %ld \n", words_in);
            word_end = false;
        }
    }

    printf("\n");
    printf("Words read in from file: %ld\n", words_in);
    printf("Last word read in: %s\n", input_string);

    while ((menu_choice = get_choice()) != 'q') {
        printf("\n");
        switch (menu_choice) {
            case 'l' : list_all_words(&word_list);
                       break;
            case 'r' : one_word_report(&word_list);
                       break;
            case 'q' :
                       break;
            default  : printf("\n*** Please enter a valid menu choice. ***\n");
                       break;
        }
    }

    printf("\nBye.\n");
    
    fclose(fp);

    puts("\n");

    return (0);
} 

void word_to_tree(const char * word, Tree * list_of_words)
{
    Item temp;

    strncpy(temp.word, word, SLEN); 
    temp.occurances = 0;
    
    if (TreeIsFull(list_of_words))
        printf("Word List is full\n");
    else
        AddItem(&temp, list_of_words);
} 

void list_all_words(const Tree * word_list)
{
    if (TreeIsEmpty(word_list))
        puts("No words present to list");
    else {
        puts("word, number of occurances\n");
        Traverse(word_list, print_word_info);
    }
}

void print_word_info(Item item)
{
    printf("%s, %ld\n", item.word, item.occurances);
}

void one_word_report(const Tree * word_list)
{
    Item temp;

    if (TreeIsEmpty(word_list))
        puts("No words present to search");
    else {
        puts("Type in the word you are searching for");
        s_gets(temp.word, SLEN);
        if (InTree(&temp, word_list)) {
            puts("\nword, number of occurances\n");
            SingleItemFunc(word_list, temp, print_word_info);
        }
        else
            printf("%s is not in the list of words\n", temp.word);
    }
}

char get_choice(void)
{
    char ch;
    printf("\nEnter the letter of your menu choice:\n");
    printf("L. List all words with number of occurences\n");
    printf("R. Report number of occurences of a word you enter\n");
    printf("Q. Quit\n");
    s_gets(&ch, 2);
    ch = tolower(ch);
}

char * s_gets(char * st, int n)
{
    char * ret_val;
    int i = 0;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        while (st[i] != '\n' && st[i] != '\0')
            i++;
        if (st[i] == '\n')
            st[i] = '\0';
        else                             // obviously we get here *only* 
                                         //    if st[i] == '\0'
            while (getchar() != '\n')    // these two lines clean out the 
                continue;                //    input buffer since we haven't
                                         //    yet found a '\n'
    }
    return ret_val;
}


/* void Traverse (const Tree * ptree, void (* pfun)(Item item))
{
    if (ptree != NULL)
        InOrder(ptree->root, pfun);
}

void DeleteAll(Tree * ptree)
{
    if (ptree != NULL)
        DeleteAllNodes(ptree->root);
    ptree->root = NULL;
    ptree->size = 0;
} */


