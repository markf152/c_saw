/* binbit.c -- using bit operations to display binary */

#include <stdio.h>
#include <limits.h>  // for CHAR_BIT, # of bits per char

char * itobs(int, char *, int *);
void show_bstr(const char *);

int main (void)
{
    char bin_str[CHAR_BIT * sizeof(int) + 1];
    int number;

    puts("\n");

    puts("Enter integers and see them in binary.");
    puts("Non-numeric input terminates program.");
    while (scanf("%d", &number) == 1) {
//        printf("address of number is %p\n", &number);
        itobs(number, bin_str, &number);
        printf("%d is ", number);
        show_bstr(bin_str);
        putchar('\n');
    }

    puts("Bye!\n");

    return (0);

}

char * itobs(int n, char * ps, int * num)
{
    int i;
    const static int size = CHAR_BIT * sizeof(int);
// *** commented-out code is from a mistake I made (put ; right after for loop line
//    printf("n = %d\n", n);
//    printf("address of num is    %p\n", num);
//    printf("value of num is %d\n", *num);
    for (i = (size - 1); i >= 0; i--, n >>= 1) {
//        printf("value of num is %d\n", *num);
//        i++;
        ps[i] = (01 & n) + '0';  // assume ASCII or similar
//        printf("address of ps[%d] is %p\n", (i+1), &ps[i+1]);
//        printf("address of ps[%d] is %p\n", i, &ps[i]);
    }
//    printf("address of num is    %p\n", num);
//    printf("value of num is %d\n", *num);
    ps[size] = '\0';

    // printf("\nn = %d\n", n);
    // n = 99;
//    printf("address of n is      %p\n", &n);
    return ps;
}

/* show binary string in blocks of 4 */
void show_bstr(const char * str)
{
    int i = 0;

    while (str[i]) {  // i.e. not the null character
        putchar(str[i]);
        if((++i % 4 == 0) && str[i])
            putchar(' ');
    }
}

