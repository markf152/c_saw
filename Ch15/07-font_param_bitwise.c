/* 07-font_param_bitwise.c -- uses unsigned long variable to store font info:
   - Font ID:   0-255
   - Font Size: 0-127
   - Alignment: 0-  2 (Left, Center or Right)
   - Bold:      Off (0) or On (1)
   - Italic:    Off (0) or On (1)
   - Underline: Off (0) or On (1)
*/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>  // for EXIT_SUCCESS / EXIT_FAILURE
#include <string.h>  // for strncpy()
#include <limits.h>  // for CHAR_BIT

#define FONT_ID_MASK   0x000FF
#define FONT_SIZE_MASK 0x07F00
#define ALIGNMENT_MASK 0x30000
#define BOLD_BIT       0x40000
#define ITALIC_BIT     0x80000
#define UNDERLINE_BIT 0x100000

void print_menu(void);
void display_settings(const unsigned int font_info);
char * s_gets(char * st, int n);
void convert_show_bstr(unsigned int n);

// struct font_info {
//    unsigned int font_id      : 8;
//    unsigned int font_size    : 7;
//    unsigned int              : 1;
//    unsigned int alignment    : 2;
//    bool bold                 : 1;
//    bool italic               : 1;
//    bool underline            : 1;
//    unsigned int              : 3;
//};


//              underline
//              | italic 
//              | |bold
//              | ||alignment
//              | |||  unused
//              | |||  |font_size
//              | |||  ||        font_id
// 0000 0000 0000 0000 0000 0000 0000 0000 

// 0000 0000 0000 0100 0110 0000 0100 0000 
//    0    0    0    4    6    0    4    0

int main(void)
{
    unsigned int font_settings = 0x46040;
    char menu_choice = 'x';
    char input_str[4] = "";
    unsigned int num_input = 0;  // use for font_id & font size input
    char * end_ptr;  // neeed for strtol(), otherwise not used
    bool input_valid = 0;
    char temp_text[20] = ""; // temporary string variable

    puts("\n");

    while (menu_choice != 'q' && menu_choice !='Q') {
        display_settings(font_settings);
        print_menu();
        input_valid = false;
        s_gets(input_str, 2);
        menu_choice = input_str[0];
        switch (menu_choice) {
            case 'f' :
            case 'F' :
                do {
                    puts("Enter font ID (0-255)");
                    s_gets(input_str, 4);
                    num_input = (unsigned int)strtol(input_str, &end_ptr, 10);
                    if (num_input >= 0 && num_input <= 255)
			input_valid = true;
                    else
                       puts("Font ID must be 0 - 255\n");
                } while (!input_valid);
                font_settings &= ~FONT_ID_MASK;  // clear font value
                font_settings |= num_input;
                printf("Font ID set to %u\n", num_input);
                break;
            case 's' :
            case 'S' :
                do {
                    puts("Enter font ID (0-127)");
                    s_gets(input_str, 4);
                    num_input = (unsigned int)strtol(input_str, &end_ptr, 10);
                    if (num_input >= 0 && num_input <= 127)
                        input_valid = true;
                    else
                       puts("Font size must be 0 - 127\n");
                } while (!input_valid);
                font_settings &= ~FONT_SIZE_MASK;
                font_settings = font_settings | (num_input << 8);
                printf("Font size set to %u\n", num_input);
                break;
            case 'a' :
            case 'A' :
                do {
                    puts("Alignment: enter L, C, or R");
                    s_gets(input_str, 2);
                    if (strchr("LCRlcr", input_str[0])) {
                        if (input_str[0] == 'L' || input_str[0] == 'l') 
                             num_input = 0;
                        else if (input_str[0] == 'C' || input_str[0] == 'c') 
                             num_input = 1;
                        else 
                             num_input = 2;
                        font_settings &= ~ALIGNMENT_MASK;
                        font_settings = font_settings | (num_input << 16);
	                input_valid = true;
                    }
                    else
                       puts("Valid alignment entries are L, C and R\n");
                } while (!input_valid);
                printf("Alignment set to ");
                if (num_input == 0)
                    printf("left\n");
                else if (num_input == 1)
                    printf("center\n");
                else
                    printf("right\n");
                break;
            case 'b' :
            case 'B' :
                font_settings ^= BOLD_BIT;
                puts("Bold toggle");
                break;
            case 'i' :
            case 'I' :
                font_settings ^= ITALIC_BIT;
                break;
            case 'u' :
            case 'U' :
                font_settings ^= UNDERLINE_BIT;
                break;
            case 'q' :
            case 'Q' :
                puts("Ending program.  Bye!\n\n");
                exit(EXIT_SUCCESS);
            default  :
                puts("Please enter a valid menu choice ('q' to quit)");
        }
    }
    puts("\n");
    return (0);
}

void display_settings(const unsigned int font_info)
{
    unsigned int temp_info = font_info;

    printf("\n ID  SIZE ALIGNMENT    B    I    U\n");
//    printf("255  127   left       off  off  off\n");
    printf("%3u   %3u   ",
        (temp_info & FONT_ID_MASK), ((temp_info & FONT_SIZE_MASK) >> 8));
    temp_info = ((font_info & ALIGNMENT_MASK) >> 16);
    if (temp_info == 0)
        printf("left  ");
    else if (temp_info == 1)
        printf("center");
    else
        printf("right ");
    printf("    %s  %s  %s\n", 
           ((font_info & BOLD_BIT) == BOLD_BIT ? "on " : "off"),
           ((font_info & ITALIC_BIT) == ITALIC_BIT ? "on " : "off"),
           ((font_info & UNDERLINE_BIT) == UNDERLINE_BIT ? "on " : "off"));
}

void print_menu(void)
{
    puts("\nf)change font   s)change size    a)change alignment");
    puts("b)toggle bold   i)toggle italic  u)toggle underline");
    puts("q)quit\n");
    printf("Enter your choice: ");
}

char * s_gets(char * st, int n)
{
    char * ret_val;
    int i = 0;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        while (st[i] != '\n' && st[i] != '\0')
            i++;
        if (st[i] == '\n')
            st[i] = '\0';
        else                     // obviously we get here *only* if st[i] == '\0'
            while (getchar() != '\n')  // these two lines clean out the input buffer
                continue;              //  since we haven't yet found a '\n'
    }
    return ret_val;
}

/* build / print binary string in blocks of 4 for easy reading */
void convert_show_bstr(unsigned int n)
{
    int i = 0;
    const static int size = CHAR_BIT * sizeof(unsigned int);
    unsigned int mask = (1 << (size-1));


    for (i = (size - 1); i >= 0; i--, mask >>= 1) {
        putchar(((mask & n) / mask) + '0');
        if ((i % 4) == 0)
            putchar(' ');
    }
}

