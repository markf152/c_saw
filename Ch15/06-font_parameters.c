/* 06-font_parameters.c -- uses bit-field structure to store font info:
   - Font ID:   0-255
   - Font Size: 0-127
   - Alignment: 0-  2 (Left, Center or Right)
   - Bold:      Off (0) or On (1)
   - Italic:    Off (0) or On (1)
   - Underline: Off (0) or On (1)
*/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>  // for EXIT_SUCCESS / EXIT_FAILURE
#include <string.h>  // for strncpy()

struct font_info {
    unsigned int font_id      : 8;
    unsigned int font_size    : 7;
    unsigned int              : 1;
    unsigned int alignment    : 2;
    bool bold                 : 1;
    bool italic               : 1;
    bool underline            : 1;
    unsigned int              : 3;
};

void print_menu(void);
void display_settings(const struct font_info * p_font_settings);
char * s_gets(char * st, int n);

int main(void)
{
    struct font_info font_settings = 
        {127, 12, 1, 0, 1, 0};
    char menu_choice = 'x';
    char input_str[4] = "";
    unsigned int num_input = 0;  // use for font_id & font size input
    char * end_ptr;  // neeed for strtol(), otherwise not used
    bool input_valid = 0;
    char temp_text[20] = ""; // temporary string variable

    puts("\n");

    while (menu_choice != 'q' && menu_choice !='Q') {
        display_settings(&font_settings);
        print_menu();
        input_valid = false;
        s_gets(input_str, 2);
        menu_choice = input_str[0];
        switch (menu_choice) {
            case 'f' :
            case 'F' :
                do {
                    puts("Enter font ID (0-255)");
                    s_gets(input_str, 4);
                    num_input = (unsigned int)strtol(input_str, &end_ptr, 10);
                    if (num_input >= 0 && num_input <= 255)
			input_valid = true;
                    else
                       puts("Font ID must be 0 - 255\n");
                } while (!input_valid);
                font_settings.font_id = num_input;
                printf("Font ID set to %u\n", font_settings.font_id);
                break;
            case 's' :
            case 'S' :
                do {
                    puts("Enter font ID (0-127)");
                    s_gets(input_str, 4);
                    num_input = (unsigned int)strtol(input_str, &end_ptr, 10);
                    if (num_input >= 0 && num_input <= 127)
			input_valid = true;
                    else
                       puts("Font size must be 0 - 127\n");
                } while (!input_valid);
                font_settings.font_size = num_input;
                printf("Font size set to %u\n", font_settings.font_size);
                break;
            case 'a' :
            case 'A' :
                do {
                    puts("Alignment: enter L, C, or R");
                    s_gets(input_str, 2);
                    if (strchr("LCRlcr", input_str[0])) {
                        if (input_str[0] == 'L' || input_str[0] == 'l') {
                            font_settings.alignment = 0;
                            strncpy(temp_text, "left", 4);
                        }
                        else if (input_str[0] == 'C' || input_str[0] == 'c') {
                            font_settings.alignment = 1;
                            strncpy(temp_text, "center", 6);
                        }
                        else {
                            font_settings.alignment = 2; 
                            strncpy(temp_text, "right", 5);
                        }
	                input_valid = true;
                    }
                    else
                       puts("Valid alignment entries are L, C and R\n");
                } while (!input_valid);
                printf("Alignment set to %u\n", font_settings.font_size);
                break;
            case 'b' :
            case 'B' :
                font_settings.bold = !font_settings.bold;
                break;
            case 'i' :
            case 'I' :
                font_settings.italic = !font_settings.italic;
                break;
            case 'u' :
            case 'U' :
                font_settings.underline = !font_settings.underline;
                break;
            case 'q' :
            case 'Q' :
                puts("Ending program.  Bye!\n\n");
                exit(EXIT_SUCCESS);
            default  :
                puts("Please enter a valid menu choice ('q' to quit)");
        }
    }

    puts("\n");

    return (0);
}

void display_settings(const struct font_info * p_font_settings)
{
    printf("\n ID  SIZE ALIGNMENT    B    I    U\n");
//    printf("255  127   left       off  off  off\n");
    printf("%3u   %3u   ",
        p_font_settings->font_id, p_font_settings->font_size);
    if (p_font_settings->alignment == 0)
        printf("left  ");
    else if (p_font_settings->alignment == 1)
        printf("center");
    else
        printf("right ");
    printf("    %s  %s  %s\n", 
           (p_font_settings->bold == true ? "on " : "off"),
           (p_font_settings->italic == true ? "on " : "off"),
           (p_font_settings->underline == true ? "on " : "off"));
}

void print_menu(void)
{
    puts("\nf)change font   s)change size    a)change alignment");
    puts("b)toggle bold   i)toggle italic  u)toggle underline");
    puts("q)quit\n");
    printf("Enter your choice: ");
}


char * s_gets(char * st, int n)
{
    char * ret_val;
    int i = 0;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        while (st[i] != '\n' && st[i] != '\0')
            i++;
        if (st[i] == '\n')
            st[i] = '\0';
        else                     // obviously we get here *only* if st[i] == '\0'
            while (getchar() != '\n')  // these two lines clean out the input buffer
                continue;              //  since we haven't yet found a '\n'
    }
    return ret_val;
}

