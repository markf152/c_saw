/* 01-bin_string_num.c -- convert binary string to numeric value */
/* create function to do this; returns an int */
/* I'm assuming an 8-bit input */
/* Will test each character to ensure '0' or '1' before processing */

#include <stdio.h>

int bin_string_to_int(char * bin_string);

int main(void)
{
    char * pbin = "01001001";

    puts("\n");

    printf("%s = ", pbin);
    printf("%d\n", bin_string_to_int(pbin));

    puts("\n");

    return (0);
}

int bin_string_to_int(char * bin_string)
{
    int i = 0;  // index variable for loops
    int num_val = 0;  // return value
    int power = 1;  // counter to keep place value of bin string during loop



    // printf("'1' - '0' = %d\n\n", ('1'-'0'));
    for (i = 7; i >= 0; i--) {
        if (bin_string[i] - '0') 
            num_val += power;
        power *= 2;
        // printf("bit#: %d;  value: %c;  power:  %d;  value: %d\n", i, bin_string[i], (power/2), num_val);
    }
    
    return (num_val);
}

