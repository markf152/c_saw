/* 03-on_bit_count.c -- takes an int argument
   returns number of 'on' bits in that argument  */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int on_bit_count(const int num);
void convert_show_bstr(int n);

int (main(int argc, char * argv[]))
{
    int num = 0;
    char * end_ptr;  // needed for strtol() function

    if (argc !=2) {
        fprintf(stderr, "Usage: %s <integer>\n\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if (num = strtol(argv[1], &end_ptr, 10)) {
        convert_show_bstr(num);
        printf("\n\nNumber of bits 'on' in %d is %d\n\n\n", num, on_bit_count(num));
    }
    else {
        fprintf(stderr, "Command line argument must be an integer.\n\n\n");
        exit(EXIT_FAILURE);
    }

}

int on_bit_count(const int num)
{
    int i = 0;   // index variable / loop counter
    int n = num;
    int on_bits = 0;

    for (i = 0; i < (sizeof(int) * CHAR_BIT); i++, n >>= 1) {
//        if (n & 1)
//            on_bits++;
        on_bits += (n & 1);
    }

    return (on_bits);
}

/* print binary string in blocks of 4 for easy reading */
void convert_show_bstr(int n)
{
    int i = 0;
    const static int size = CHAR_BIT * sizeof(int);

    for (i = (size - 1); i >= 0; i--, n >>= 1) {
        putchar((01 & n) + '0');
        if ((i % 4) == 0)
            putchar(' ');
    }
    puts("\n");
}

