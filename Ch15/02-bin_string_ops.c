/* 02-bin_string_ops.c -- 
   - reads 2 binary strings as command line arguments
   - prints results of applying ~ operator to each
   - prints results of applying &, | and ^ operators to the pair
     (all results printed as binary strings)
   - problem loc ~ 19045
   - command line arguments . . . loc 13615
*/

#include <stdio.h>
#include <limits.h>
#include <string.h>

char * itobs(int n, char * ps);
void show_bstr(const char * str);

int main(int argc, char * argv[])
{
    char bin_str[CHAR_BIT * sizeof(int) + 1]; // CHAR_BIT = length of a byte
    int arg_int[3] = {0, 0, 0};  // int value  binary strings from command line
    char arg_bin_str[3][33];
    int arg_len[3] = {0, 0, 0}; // first element (0) not used - keep it simple
    int i = 0; // index variable - loop counter
    int j = 0; // index variable - loop counter
    int power = 1;  // tracks powers of two during loop through 8 bits
    char result_str[33];

    puts("\n");

    if (argc != 3) {
        printf("argc = %d\n", argc);
        puts("Incorrect number of command line arguments . . .");
        puts(". . . two strings of up to 32 '1' or '0' (zero) characters");
        return (1);
    }

    arg_len[1] = (int)strlen(argv[1]);
    arg_len[2] = (int)strlen(argv[2]); 
    printf("length of argv[1] = %d   length of argv[2] = %d\n\n", 
             arg_len[1], arg_len[2]);
    for (i = 1; i <= 2; i++) {
        if (arg_len[i] > 32)
            arg_len[i] = 32;
    }

    // push input shorter than 32 bits to the right of 32 char array
    // pad unused bits to the left with '0'
    // any non '1' character encountered becomes a '0'
    for (i = 1; i <= 2; i++) {
        for (j = 0; j < 32; j++) {
            if (j >= (32 - arg_len[i])) {
                arg_bin_str[i][j] = argv[i][j-(32-arg_len[i])];
                if (!(arg_bin_str[i][j] == '0' || arg_bin_str[i][j] == '1'))
                    arg_bin_str[i][j] = '0';  // all non '1' chars become '0'!
            }
            else
                arg_bin_str[i][j] = '0';
        }
        arg_bin_str[i][32] = '\0';
    }

    // build integer value of each argument bit by bit   
    for (i = 1; i <= 2; i++) {
        power = 1;
        for (j = 31; j >= 0; j--) {
            if (arg_bin_str[i][j] == '1')
                arg_int[i] += power;
            power *= 2;
        }
    }

    // print out integer values of each argument for testing
    printf("arg_int[1] = %d        arg_int[2] = %d\n\n", 
             arg_int[1], arg_int[2]);        
    
    // print one's complement / bitwise negation for each argument
    for (i  = 1; i <= 2; i++) {
       printf("1's complement (~) of ");
        show_bstr(arg_bin_str[i]);
        printf("\n                    = ");
        show_bstr(itobs((~arg_int[i]), result_str));
        puts("\n");
    }

    // print bitwise AND of the two arguments
    printf("Bitwise AND (&)       ");
    show_bstr(arg_bin_str[1]);
    printf("\n                    & ");
    show_bstr(arg_bin_str[2]);
    printf("\n                    = ");
    show_bstr(itobs((arg_int[1] & arg_int[2]), result_str));
    puts("\n");

    // print bitwise OR of the two arguments
    printf("Bitwise OR (|)        ");
    show_bstr(arg_bin_str[1]);
    printf("\n                    | ");
    show_bstr(arg_bin_str[2]);
    printf("\n                    = ");
    show_bstr(itobs((arg_int[1] | arg_int[2]), result_str));
    puts("\n");

    // print bitwise EXCLUSIVE OR (XOR) of the two arguments
    printf("Bitwise XOR (^)       ");
    show_bstr(arg_bin_str[1]);
    printf("\n                    ^ ");
    show_bstr(arg_bin_str[2]);
    printf("\n                    = ");
    show_bstr(itobs((arg_int[1] ^ arg_int[2]), result_str));
    puts("\n");

    return (0);
}

/* convert int to a binary string */
char * itobs(int n, char * ps)
{
    int i;
    const static int size = CHAR_BIT * sizeof(int);

    for (i = (size - 1); i >= 0; i--, n >>= 1)
        ps[i] = (01 & n) + '0';
    ps[size] = '\0';

    return (ps);
}

/* print binary string in blocks of 4 for easy reading */
void show_bstr(const char * str)
{
    int i = 0;

    while (str[i]) {
        putchar(str[i]);
        if ((++i % 4) == 0 && str[i])
            putchar(' ');
    }
}

