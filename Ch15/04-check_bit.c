/* 04-check_bit.c -- takes two int command line arguments
   (1) a value and (2) a bit position
   returns status of the bit at the specified position:
   1 if the bit is on; 0 if it is off  */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int bit_status(const int num, const int bit_num);

int (main(int argc, char * argv[]))
{
    int num = 0;
    int bit_num = 0;
    char * end_ptr;  // needed for strtol() function

    if (argc !=3) {
        fprintf(stderr, "Usage: %s <integer value> <bit position> (0-31)\n\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if ((num = strtol(argv[1], &end_ptr, 10)) && ((bit_num = strtol(argv[2], &end_ptr, 10)) >= 0)) {
        printf("\n\nFor int %d, the status of bit %d is %d\n\n\n", num, bit_num, bit_status(num, bit_num));
    }
    else {
        fprintf(stderr, "Command line arguments must be an integers.\n\n\n");
        exit(EXIT_FAILURE);
    }

}

int bit_status(const int num, const int bit_num)
{
    int i = 0;   // index variable / loop counter
    int n = num;

    for (i = 0; i <  bit_num; i++, n >>= 1);  // note ';' - this loop just shifts the bits
    return (n & 1);
}

