/* 05-rotate_bits_left.c -- takes an unsigned int argument and
   an int argument
   - rotates the bits in the first argument left the number of 
   places specified by the second argument
   - bits that rotate out of the left end reappear on the right
   - returns the new number */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

unsigned int rotate_bits_left(const unsigned int num, const unsigned int bits_left);
void convert_show_bstr(unsigned int n);

int (main(int argc, char * argv[]))
{
    unsigned int num = 0;
    unsigned int bits_left = 0;
    unsigned int result = 0;
    char * end_ptr;  // needed for strtol() function

    puts("\n");

   if (argc !=3) {
        fprintf(stderr, "Usage: %s <unsigned integer> <positive integer>\n\n", argv[0]);
        exit(EXIT_FAILURE);
    }

//    printf("2147483648 << 1 = %u\n", ((unsigned int)2147483648 << 1));
//    printf("0 | 1 = %d\n", (0 | 1));

    if ((num = strtol(argv[1], &end_ptr, 10)) && 
       ((bits_left = strtol(argv[2], &end_ptr, 10)) >= 0)) {
        printf("%10u = ", num);
        convert_show_bstr(num);
        printf("\n                bit-shifted left %u place", bits_left);
        if (bits_left > 1)
            printf("s\n");
        else
            printf("\n");
        result = rotate_bits_left(num, bits_left);
        printf("%10u = ", result);
        convert_show_bstr(result);
    }
    else {
        fprintf(stderr, "Command line argument must be an integer.\n\n\n");
        exit(EXIT_FAILURE);
    }
    puts("\n");

}

unsigned int rotate_bits_left(const unsigned int num, const unsigned int bits_left)
{
    int i = 0;               // index variable / loop counter
    unsigned int n = num;    // local variable so we can rotate bits
    int bit_zero = 0;        // flag 
    const static int size = CHAR_BIT * sizeof(unsigned int);
    const unsigned int mask = (1 << (size-1));

    for (i = 0; i < bits_left; i++) {
        n = (n | bit_zero);  // wrap left bit into bit zero if it was 'on'
        bit_zero = 0;
        if (n & mask) {      // check next left bit is on)
            bit_zero = 1;    // mask = 2147483648 = 1000 0000 0000 0000 0000 0000 0000 0000 
        }                    // (for 32 bit unsigned int -- might be different on other systems)
        n <<= 1;
    }
    n |= bit_zero;           // check to see if there is that last bit to wrap to 0
    return (n);
}

/* build / print binary string in blocks of 4 for easy reading */
void convert_show_bstr(unsigned int n)
{
    int i = 0;
    const static int size = CHAR_BIT * sizeof(unsigned int);
    unsigned int mask = (1 << (size-1));


    for (i = (size - 1); i >= 0; i--, mask >>= 1) {
        putchar(((mask & n) / mask) + '0');
        if ((i % 4) == 0)
            putchar(' ');
    }
}

