﻿// 08-airTEST.c -- Seating reservation program for Colossus Airlines
//
// prog exercise . . . loc 18159
// menu techniques . . .loc 9154 / text pg 324
//
// Flights are represented
//  Individual flights is represented by an array of structures, one for each seat:
//  - seat ID number
//  - marker - seat assigned or not
//  - last name of seat holder
//  - first name of seat holder
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>  // need for access() function to check existance of file
#include <stdbool.h>
#include <ctype.h>

#define FLIGHT_LIST_FILE_NAME "flightList.dat" // flight list file name
#define FLIGHT_LIST_FILE_NAME_BAK "flightListbak.dat" // back-up file name

#define MAX_NAME_LEN 40
#define MAX_CITY_NAME_LEN 40
#define MAX_FLIGHTS 500
#define NUMSEATS 12
#define MAXSEATS 500
#define MAX_FLIGHTS_PER_MENU_PAGE 10

struct flightInfo {
    char flightNum[5];
    int numSeats;
    char fromCity[MAX_CITY_NAME_LEN];
    char toCity[MAX_CITY_NAME_LEN];
    char confirm;
};

struct seat {
    int seatID;                // 1 through NUMSEATS (0 is not used)
    int seatAssigned;          // 0 = not assigned; non-zero = assigned
    int seatConfirmed;
    char paxLname[MAX_NAME_LEN];
    char paxFname[MAX_NAME_LEN];
};

//Utility Functions . . .
char * s_gets(char * st, int n);
int getfile(char * fileName, FILE ** filePtr, char createYN);
    // createYN: Y = try to create if it doesn't exists; N = don't try
void createEmptySeats(FILE ** flt, int numSeats);
void endProgram();       // 'q' == QUIT
void strngcpy(char * dest, char * src);
int backUpFile(char * dest, char * src);

//Menu Functions . . .
char mainMenu(void);
int flightListMenu(struct flightInfo *listOfFlights, int numFlights);
int editFlightMenu(struct flightInfo flight);

//Show, Add, Delete Flights . . .
int sortByFltNum(struct flightInfo * listOfFlights, int numFlights);
int sortByFromCity(struct flightInfo * listOfFlights, int numFlights);
int sortByToCity(struct flightInfo * listOfFlights, int numFlights);
int addFlight(struct flightInfo * listOfFlights, int numFlights, char * flightList);
int getExistingFlights(FILE ** flightListFile,
                         struct flightInfo * flightList);
int deleteFlight(struct flightInfo * listOfFlights, int numFlights,
                   char * flightList);

//Functions for individual flight work . . .
void loadFlightData(FILE ** flt, struct seat * plane, int seats);
void showNrEmptySeats(const struct seat * plane, int numSeats);
void listEmptySeats(const struct seat * flt, int numSeats);
void listFlightPax(const struct seat * plane, int numSeats, int howList);
void assignPaxSeat(struct seat * plane, int numSeats);
void deleteSeatAssign(struct seat *);


int main (void)
{
    struct seat plane1[NUMSEATS];
    struct flightInfo listOfFlights[MAX_FLIGHTS];

    FILE * flightListFile;
    FILE * flightFile;

    int seatSize = sizeof(struct seat);
    int flightInfoSize = sizeof(struct flightInfo);
    int menuChoice;
    int i; // generic loop counter
    int numFlights;  // total number of flights for airline

    puts("\n");

    getfile(FLIGHT_LIST_FILE_NAME, &flightListFile, 'Y');
    numFlights = 0;
    while (numFlights < MAX_FLIGHTS && fread(&listOfFlights[numFlights],
           flightInfoSize, 1, flightListFile) == 1) {
        numFlights++;
    }
    fclose(flightListFile);

    flightListMenu(listOfFlights, numFlights);

    puts("\n");

}


//****************************************************************************
int getfile(char * fileName, FILE ** filePtr, char createYN)
// createYN . . . Y = try to create if it doesn't exists; N = don't try
{
    int i;

    if ((*filePtr = fopen(fileName, "r+b")) == NULL) {
        printf("File %s can't be opened for update, will check existence and try to create\n", fileName);
        if (access(fileName, F_OK) == -1) {
            printf("File %s fails 'access()' existence check\n", fileName);
            if (createYN = 'Y') {
                if ((*filePtr = fopen(fileName, "w+b")) == NULL) {
                    fprintf(stderr, "Can't open or create '%s' file.\n\n",
                          fileName);
                    return 2;
                }
                else {
                    printf("File %s created\n\n",fileName);
                    return 1;  // 1 = new file created and opened
                }
            }
        }
        else {
            printf("File %s exists, but can't be opened for update.  Program ending.\n",fileName);
            exit(EXIT_FAILURE);
        }
    }
    else
//        printf("File %s is opened for update.\n", fileName);
        return 0;  // 0 = file exists and was opened successfully

//    printf("flt = %p\n", *flt);
//    puts("ok rewind . . .");
}


//****************************************************************************
void createEmptySeats(FILE ** flt, int numSeats)
{
    int seatSize = sizeof(struct seat);
    struct seat emptySeat = {0, 0, 0, 0, 'N'};
    int i;  // loop variable

    rewind(*flt);

    for (i = 0; i < numSeats; i++) {  // create each empty seat
        emptySeat.seatID = i + 1;

        if (((i + 1) % 2) || ((i + 1) == 8)) // temp for testing
            emptySeat.seatAssigned = 1; // temp for testing
        else                            // temp for testing
            emptySeat.seatAssigned = 0; // temp for testing

//        printf("Seat %2d: %d\n", (i + 1), emptySeat.seatAssigned);

        fwrite(&emptySeat, seatSize, 1, *flt);
    }
}

//****************************************************************************
void loadFlightData(FILE ** flt, struct seat * plane, int seats)
{
    int i;
    int x;   // temp for testing
    int seatSize = sizeof(struct seat);
//    printf("plane = %p\n", plane);

    rewind(*flt);
//    printf("flt = %p\n", *flt);

    for (i = 0; i < seats; i++) {
//        printf("i = %2d  ", i);
//        x = fread(plane++, seatSize, 1, *flt);
//        printf("x = %2d\n", x);
        if (fread(plane++, seatSize, 1, *flt) != 1) {
            puts("Error . . . couldn't read in all seats for flight.");
            puts("Contact tech support.  Program ending.");
            puts("Function = loadFlightData");
            exit(1);
        }

/* This entire section for testing . . .
        plane--;
        printf("file location: %ld\n", ftell(*flt));
        printf("seatID: %d\n", plane->seatID);
        printf("seatAssigned: %d\n", plane->seatAssigned);
        printf("paxLname: %s\n", plane->paxLname);
        printf("paxFname: %s\n", plane->paxFname);
        plane++;
*/

    }
//    puts("read in all seats");
}


//****************************************************************************
char mainMenu(void)
{
    puts("Select a function (enter its letter)...");
    puts("a) Pick a flight");
    puts("b) Create a flight");
    puts("c) Delete a flight");
    puts("q) Quit");
}


//****************************************************************************
int flightListMenu(struct flightInfo * listOfFlights, int numFlights)
{
    int flightInfoSize = sizeof(struct flightInfo);
    int i = 0; // general index variable
    int j = 0; // general index variable
    int fltNum; // temp flight number variable
    int page = 0; // marker to track what part of list of flights is displayed
    char menuChoice[5] = "xxxx";
    char * junkPtr;  // pointer needed for strtol() - not used otherwise
    char quit[3] = "x";
    bool validMenuChoice = false;
    char * temp;  // needed for strtol() -- not used otherwise

    while (quit[0] != 'Y' && quit[0] != 'y') {
        puts("Flt Nr   From                To");
        puts("--------------------------------------------------------------");
        for (i = page; (i < numFlights) && (i < (page + 10)); i++) {
            printf("%4s     %s",
                listOfFlights[i].flightNum,
                listOfFlights[i].fromCity);
            for (j = 0; j < (20 - strlen(listOfFlights[i].fromCity)); j++)
                printf(" ");
            printf("%s\n",listOfFlights[i].toCity);
        }

        if (i == 0) // no flights stored in flightList file
            printf("<<< No flights in the current data file >>>\n");
        printf("\n");

        puts("(L)Next screen / (P)revious screen");
        puts("Sort by Flight (N)umber / (F)rom City / (T)o City");
        puts("Enter flight number / (A)dd flight / (D)elete flight / (Q)uit");

        s_gets(menuChoice, 5);
        if (isalpha(menuChoice[0])) {
            if (strchr("LPNFTADQlpnftadq", menuChoice[0])) {
                validMenuChoice = true;
                switch (menuChoice[0]) {
                    case 'L' :
                    case 'l' :
                        if ((page + 10) < numFlights)
                            page += 10;
                        break;
                    case 'P' :
                    case 'p' :
                        page -= 10;
                        if (page < 0)
                            page = 0;
                        break;
                    case 'N' :
                    case 'n' :
                        // sort by flight number
                        sortByFltNum(listOfFlights, numFlights);
                        break;
                    case 'F' :
                    case 'f' :
                        // sort by From city
                        sortByFromCity(listOfFlights, numFlights);
                        break;
                    case 'T' :
                    case 't' :
                        // sort by To city
                        sortByToCity(listOfFlights, numFlights);
                        break;
                    case 'A' :
                    case 'a' :
                        // Add a flight
                        i = addFlight(listOfFlights, numFlights,
                                      FLIGHT_LIST_FILE_NAME);
                        if (i > numFlights)
                            numFlights = i;
                        break;
                    case 'D' :
                    case 'd' :
                        // Delete a flight
                        i = deleteFlight(listOfFlights, numFlights,
                                         FLIGHT_LIST_FILE_NAME);
                        if (i < numFlights)
                            numFlights = i;
                        break;
                    case 'Q' :
                    case 'q' :
                        // Quit
                        puts("Are you sure you want shut down the program?");
                        puts("Enter 'Y' or 'y' to confirm or any other key to continue");
                        s_gets(quit, 2);  // looking for quit[0] == 'Y' or 'y' to quit
                        break;
                }
            }
        }
        else  {
            fltNum = strtol(menuChoice, &temp, 10);
            for (i = 0; i < numFlights; i++)
                if ((int)strtol(listOfFlights[i].flightNum, &temp, 10) == fltNum) {
                    printf("Valid flight number %d\n\n", fltNum);
                    validMenuChoice = true;
                    editFlightMenu(listOfFlights[i]);
                }
            if (!validMenuChoice)
                printf("Not a valid menu choice or flight number.  Try again.\n\n");
        }

    }
}


//****************************************************************************
int sortByFltNum(struct flightInfo * listOfFlights, int numFlights)
{
    struct flightInfo tempFltInfo;
    int i;  // loop counter
    int j;  // loop counter

    for (i = 0; i < (numFlights - 1); i++) {
        for (j = (i + 1); j < numFlights; j++) {
            printf("listOfFlights[j] = %s   listOfFlights[i] = %s\n",
                    listOfFlights[j].flightNum, listOfFlights[i].flightNum);
            if (strcmp(listOfFlights[j].flightNum, listOfFlights[i].flightNum) < 0) {
                tempFltInfo = listOfFlights[i];
                listOfFlights[i] = listOfFlights[j];
                listOfFlights[j] = tempFltInfo;
            }
        }
    }
}


//****************************************************************************
int sortByFromCity(struct flightInfo * listOfFlights, int numFlights)
{
    struct flightInfo tempFltInfo;
    int i;  // loop counter
    int j;  // loop counter

    for (i = 0; i < (numFlights - 1); i++) {
        for (j = (i + 1); j < numFlights; j++) {
            if (strcmp(listOfFlights[i].fromCity, listOfFlights[j].fromCity) > 0) {
                tempFltInfo = listOfFlights[i];
                listOfFlights[i] = listOfFlights[j];
                listOfFlights[j] = tempFltInfo;
            }
        }
    }
}


//****************************************************************************
int sortByToCity(struct flightInfo * listOfFlights, int numFlights)
{
    struct flightInfo tempFltInfo;
    int i;  // loop counter
    int j;  // loop counter

    for (i = 0; i < (numFlights - 1); i++) {
        for (j = (i + 1); j < numFlights; j++) {
            if (strcmp(listOfFlights[i].toCity, listOfFlights[j].toCity) > 0) {
                tempFltInfo = listOfFlights[i];
                listOfFlights[i] = listOfFlights[j];
                listOfFlights[j] = tempFltInfo;
            }
        }
    }
}


//****************************************************************************
int addFlight(struct flightInfo * listOfFlights, int numFlights, char * flightList)
{
    struct flightInfo newFlight;
    struct flightInfo tempFlight; // storage location for sorting algorithm
    int fltNum = 0;
    int seats = 0;
    char fmCity[MAX_CITY_NAME_LEN];
    char newFlightFileName[16];
    char toCity[MAX_CITY_NAME_LEN];
    int i = 0;    // loop counter
    int j = 0;    // loop counter
    char string[50];
    char sfltNum[5];
    char * temp;  // needed for strtol() -- not used otherwise
    FILE * tempFlightFile;  // for creating the new individual flight file
    FILE * flightListFilePtr;  // for opening the flight list file

    printf("Create a new flight (hit only <return> on any line to abort and return to previous menu)\n\n");

    do {
        printf("Enter the new flight number (1 - 9999) . . . ");
        s_gets(string, 5);
        if (strlen(string) == 0) {
            puts("Terminating Add Flight function.");
            return 0;
        }
        fltNum = (int)strtol(string, &temp, 10);
        if (fltNum > 0 && fltNum < 10000) {  // check for valid flt nr . . .
            i = 1;
            for (j = 0; j < numFlights; j++) {  // and chk if it already exists
                if ((int)strtol(listOfFlights[j].flightNum, &temp, 10) == fltNum) {
                    printf("flight %d already exists.\n", fltNum);
                    i = 0;
                }
            }
        }
        printf("\n");
    } while (i == 0);
    strngcpy(sfltNum, string);

    // this section builds the flight file name using the flight number
    // it pads the file name with zeros to aid in trouble-shooting when
    // listing files

//    printf("The current HOME is %s \n\n", getenv("HOME"));
    printf("string = %s\n", string);
    strngcpy(string, sfltNum);
    sfltNum[0] = '\0';
    newFlightFileName[0] = '\0';
    printf("length of string = %d\n", (int)(strlen(string)));
    switch ((int)strlen(string)) {
        case 1 :
            strncat(newFlightFileName, "08-flights/000", 14);
            strncat(newFlightFileName, string, 1);
            strncat(sfltNum, "000", 3);
            strncat(sfltNum, string, 4);
            printf("newFlightFileName = %s\n\n", newFlightFileName);
            break;
        case 2 :
            strncat(newFlightFileName, "08-flights/00", 13);
            strncat(newFlightFileName, string, 2);
            strncat(sfltNum, "00", 2);
            strncat(sfltNum, string, 4);
            printf("newFlightFileName = %s\n\n", newFlightFileName);
            break;
        case 3 :
            strncat(newFlightFileName, "08-flights/0", 12);
            strncat(newFlightFileName, string, 3);
            strncat(sfltNum, "0", 1);
            strncat(sfltNum, string, 4);
            printf("newFlightFileName = %s\n\n", newFlightFileName);
            break;
        case 4 :
            strncat(newFlightFileName, "08-flights/", 11);
            strncat(newFlightFileName, string, 4);
            strncat(sfltNum, string, 4);
            printf("newFlightFileName = %s\n\n", newFlightFileName);
            break;
    }

    i = 0;
    do {
        printf("Enter the number of seats on flight %d . . . ", fltNum);
        s_gets(string, 5);
        if (strlen(string) == 0) {
            puts("Terminating Add Flight function.");
            return 0;
        }
        seats = (int)strtol(string, &temp, 10);
        if (seats >= 1 && seats < 500)
            i = 1;
        printf("\n");
    } while (i == 0);

    i = 0;
    do {
        printf("Enter the name of the departure city for flight %d . . . ", fltNum);
        s_gets(fmCity, 49);
        if (strlen(fmCity) == 0) {
            puts("Terminating Add Flight function.");
            return 0;
        }
        if (strlen(fmCity) > 0)
            i = 1;
    } while (i == 0);

    i = 0;
    do {
        printf("Enter the name of the destination city for flight %d . . . ", fltNum);
        s_gets(toCity, 49);
        if (strlen(toCity) == 0) {
            puts("Terminating Add Flight function.");
            return 0;
        }
        if (strlen(toCity) > 0)
            i = 1;
    } while (i == 0);

    strngcpy(listOfFlights[numFlights].flightNum, sfltNum);
    listOfFlights[numFlights].numSeats = seats;
    strngcpy(listOfFlights[numFlights].fromCity, fmCity);
    strngcpy(listOfFlights[numFlights].toCity, toCity);
    numFlights++;

    backUpFile(FLIGHT_LIST_FILE_NAME, FLIGHT_LIST_FILE_NAME_BAK);

    flightListFilePtr = fopen(FLIGHT_LIST_FILE_NAME, "wb");
    if (!flightListFilePtr) {
        fputs("Error opening flight list file for writing.  Ending Program", stderr);
        exit(EXIT_FAILURE);
    }

    j = fwrite(listOfFlights, sizeof(struct flightInfo), numFlights, flightListFilePtr);
    if (j != numFlights) {  //test to make sure all flights were written
            fputs("Error - not all flights written to file\n", stderr);
            exit(EXIT_FAILURE);
    }

    fclose(flightListFilePtr);

    printf("\n\n");
    printf("numFlights = %d\n\n", numFlights);
    printf("For new flight number %s . . .\n", listOfFlights[numFlights].flightNum);
    printf("The number of seats is %d\n", listOfFlights[numFlights].numSeats);
    printf("The departure city is %s\n", listOfFlights[numFlights].fromCity);
    printf("The destination city is %s\n\n", listOfFlights[numFlights].toCity);

    i = getfile(newFlightFileName, &tempFlightFile, 'Y');
    if (i != 1)
        switch (i) {
            case 0 :
                puts("A flight file for that flight number already exists\n");
                puts("Delete that file and try to create the new flight again.\n");
                puts("Terminating Add Flight function.\n");
                return 0;
            case 2 :
                puts("The new flight file can't be created\n");
                puts("There is a system problem.  Program ending.\n");
                exit(EXIT_FAILURE);
            default :
                puts("An unspecified error occured.  Program ending.\n");
                exit(EXIT_FAILURE);
        }
    createEmptySeats(&tempFlightFile, seats);
    fclose(tempFlightFile);
    return (numFlights);
}


//****************************************************************************
int deleteFlight(struct flightInfo * listOfFlights, int numFlights, char * flightList)
{
    char string[50];
    int fltNum = 10000;
    int i = 0; // index variable
    int j = 0; // index variable
    int done = 0;
    char * temp;  // needed for strtol() -- not used otherwise
    FILE * flightListFilePtr;  // for opening the flight list file

    while (!done) {
        printf("Enter the number of the flight to delete <return> to abort: ");
        s_gets(string, 5);
        if (strlen(string) == 0) {
            puts("Aborting delete flight");
            return(numFlights);
        }
        fltNum = (int)strtol(string, &temp, 10);
        printf("fltNum = %d\n", fltNum);
        if (fltNum >= 0 && fltNum < 10000) {  // check for valid flt nr . . .
            // . . . and step through until we find it
            for (i = 0, j = 0; i < numFlights; i++, j++) {
                if ((int)strtol(listOfFlights[i].flightNum, &temp, 10) == fltNum) {
                    printf("You have selected this flight to delete:\n");
                    printf("Flight Number: %s\n", listOfFlights[i].flightNum);
                    printf("From: %s\n", listOfFlights[i].fromCity);
                    printf("To: %s\n", listOfFlights[i].toCity);
                    printf("Seats: %d\n", listOfFlights[i].numSeats);
                    done = 1;
                    j++;
                }
                printf("i = %d   j = %d\n", i, j);
                if (i != j) {
                    strngcpy(listOfFlights[i].flightNum, listOfFlights[j].flightNum);
                    strngcpy(listOfFlights[i].fromCity, listOfFlights[j].fromCity);
                    strngcpy(listOfFlights[i].toCity, listOfFlights[j].toCity);
                    listOfFlights[i].numSeats = listOfFlights[j].numSeats;
                }
            }
        }

        if (done)
            numFlights--;
        else
            puts("Flight number entered is not valid.");
    }

    backUpFile(FLIGHT_LIST_FILE_NAME, FLIGHT_LIST_FILE_NAME_BAK);
    flightListFilePtr = fopen(FLIGHT_LIST_FILE_NAME, "wb");
    if (!flightListFilePtr) {
        fputs("Error opening flight list file for writing.  Terminating program", stderr);
        exit(EXIT_FAILURE);
    }
    i = fwrite(listOfFlights, sizeof(struct flightInfo), numFlights,
               flightListFilePtr);
    if (i != numFlights)
        fputs("Error - not all flights written to file\n", stderr);
    return (numFlights);
}


//****************************************************************************
int editFlightMenu(struct flightInfo flight)
{
    struct seat seats[MAXSEATS];
    int seatSize = sizeof(struct seat);
    int sort = 0; // 0 = by flt num
                  // 1 = by passenger last name
                  // 2 = by empty / seat nr, then reserved / seat nr
    int i; //index variable for loops

    FILE * flightFile;  // flight file
    char flightFileName[15] = "08-flights/";
    char menuChoice[3];
    bool validMenuChoice = false;

    // to build file name, convert fltNum to string
    //    using sprintf()
    // build rest of file name (pad w/ 0's as needed)

    printf("Flight Number is %s\n\n", flight.flightNum);
    printf("Length of Flight Nr variable is: %d\n", (int)strlen(flight.flightNum));

    // open the flight file . . .
    strncat(flightFileName, flight.flightNum, 4);
    flightFile = fopen(flightFileName, "rb+");
    if (!flightFile) {
        puts("Something went wrong . . . .");
    }

    // read in the seats for the flight . . .
    for (i = 0; i < flight.numSeats; i++) {
        if (fread(&seats[i], seatSize, 1, flightFile) != 1) {
            fputs("Error reading in flight info", stderr);
            fclose(flightFile);
            return (1);
        } 
    }

    fclose(flightFile);

    // *** THIS LINE FOR TESTING . . . 
    listFlightPax(seats, flight.numSeats, 0);

    printf("\nWorking Flight Nr %s\n\n", flight.flightNum);
    puts("Choose a function (enter its letter)...");
    puts("a) Show number of empty seats");
    puts("b) Show list of empty seats");
    puts("c) Show alphabetical list of seats");
    puts("d) Assign a customer to a seat");
    puts("e) Confirm a customer's seat assignment");
    puts("f) Delete a seat assignment");
    puts("r) Return to Main Flights Menu\n");
    
    s_gets(menuChoice, 2);
    if (isalpha(menuChoice[0])) {
        if (strchr("abcdefrABCDEFR", menuChoice[0])) {
            validMenuChoice = true;
            switch (menuChoice[0]) {
                case 'a' :
                case 'A' :
                    showNrEmptySeats(seats, flight.numSeats);
                    break;
                case 'b' :
                case 'B' :
                    listEmptySeats(seats, flight.numSeats);
                    break;
                case 'c' :
                case 'C' :
                    listFlightPax(seats, flight.numSeats, 1);
                    break;
                case 'd' :
                case 'D' :
                    assignPaxSeat(seats, flight.numSeats);
                    listFlightPax(seats, flight.numSeats, 1);
                    break;
                case 'e' :
                case 'E' :
                    break;
                case 'f' :
                case 'F' :
                    break;
                case 'r' :
                case 'R' :
                    fclose(flightFile);
                    return(0);
            }
        }
    }
}

/*    backUpFile(FLIGHT_LIST_FILE_NAME, FLIGHT_LIST_FILE_NAME_BAK);

    flightListFilePtr = fopen(FLIGHT_LIST_FILE_NAME, "wb");
    if (!flightListFilePtr) {
        fputs("Error opening flight list file for writing.  Ending Program", stderr);
        exit(EXIT_FAILURE);
    }

    j = fwrite(listOfFlights, sizeof(struct flightInfo), numFlights, flightListFilePtr);
    if (j != numFlights) {  //test to make sure all flights were written
            fputs("Error - not all flights written to file\n", stderr);
            exit(EXIT_FAILURE);
    }

    fclose(flightListFilePtr);
*/

//****************************************************************************
int getExistingFlights(FILE ** flightListFile,
                         struct flightInfo * flightList)
// returns number of existing flights (i)
{
    int i = 0; // increments to count number of flights listed in
               // file pointed to by flightList

    rewind(*flightListFile);
    while (fread(&flightList[i], (sizeof(struct flightInfo)),
           1, *flightListFile) == 1) {
        i++;
    }
    return i;
}


//****************************************************************************
void showNrEmptySeats(const struct seat * plane, int numSeats)
{
    int i;
    int filled = 0;

    for (i = 0; i < numSeats; i++) {
//        printf("plane[%d].seatID = %d\n", i, plane->seatID);
//        printf("plane[%d].seatAssigned = %d\n", i, plane->seatAssigned);
        if (plane++->seatAssigned)
            filled++;
    }
    printf("%d seats are currently filled.\n\n", filled);
}


//****************************************************************************
void listEmptySeats(const struct seat * plane, int numSeats)
{
    int i;
    int firstEmptyPrinted = 0;

    printf("The following seats are empty:\n");

    for (i = 0; i < numSeats; i++)  {
        if (!(plane->seatAssigned)) {
            if (firstEmptyPrinted)
                printf(", ");
            printf("%d", (plane->seatID + 1));
            firstEmptyPrinted++;
        }
        plane++;
    }

    printf("\n\n");
}


//****************************************************************************
void listFlightPax(const struct seat * plane, int numSeats, int howList)
{
    int i;
    int j;
    int temp; // temp storage during sort
    int *  sortedSeats;
    sortedSeats = (int *)malloc(numSeats * sizeof(int));

    for (i = 0; i < numSeats; i++)
        sortedSeats[i] = i;

    for (i = 0; i < (numSeats - 1); i++) {
        for (j = (i + 1); j < numSeats; j++) {
            printf("%s   %s\n", plane[j].paxLname, plane[j-1].paxLname);
            if (strcmp(plane[j].paxLname, plane[j-1].paxLname) > 0) {
                temp = sortedSeats[i];
                sortedSeats[i] = sortedSeats[j];
                sortedSeats[j] = temp;
            }
        }
    }

    puts("Last Name                 First Name           Seat Nr  Confirm");
    puts("---------------------------------------------------------------");       
    
    switch (howList) {
        case 1 :
            for (i = 0; i < numSeats; i++) {
                printf("%s", plane[(sortedSeats[i])].paxLname);
                for (j = 0; j < (25 - strlen(plane[(sortedSeats[i])].paxLname)); j++)
                    printf(" ");
                printf("%s", plane[(sortedSeats[i])].paxFname);
                for (j = 0; j < (20 - strlen(plane[(sortedSeats[i])].paxFname)); j++)
                    printf(" ");
                printf("%4d     ", (plane[(sortedSeats[i])].seatID));
                if (plane[(sortedSeats[i])].seatConfirmed)
                    printf("   Y\n");
                else
                    printf("   N\n");
            }
            break;
        case 0 :
        default : 
            for (i = 0; i < numSeats; i++) {
                printf("%s", plane[i].paxLname);
                for (j = 0; j < (25 - strlen(plane[i].paxLname)); j++)
                    printf(" ");
                printf("%s", plane[i].paxFname);
                for (j = 0; j < (20 - strlen(plane[i].paxFname)); j++)
                    printf(" ");
                printf("%4d     ", (plane[i].seatID +1));
                if (plane[i].seatConfirmed)
                    printf("   Y\n");
                else
                    printf("   N\n");
            }
            break;
    }
}


//****************************************************************************
void assignPaxSeat(struct seat * plane, int numSeats)
{
    int seatNr = 0;
    char string[4];
    char * temp;  // needed for strtol() -- not used otherwise
    int validEntry = 0;
    int i = 0;
    struct seat emptySeat = {0, 0, 0, 0, 'N'};
    
    printf("Assign a seat . . .\n");
    printf("(hit only <return> on any line to abort and return to the previous menu.\n\n");
    
    while (!validEntry) {
        printf("Enter the number of the seat you wish to assign:\n");
        s_gets(string, 4);
        if (strlen(string) == 0) {
            puts("Terminating Assign Seat function.");
            return;
        }
        if (!isdigit(string[0])) {
            puts("Please enter a valid seat number");
            continue;
        }
        else
            seatNr = strtol(string, &temp, 10);
        if ((seatNr > 0) && (seatNr <= numSeats)) {
            printf("The seat nr entered was: %d\n\n", seatNr);
        }
        else {
            puts("Please enter a valid seat number");
            continue;
        } 
        printf("Seat Nr %d assigned value is: %d\n", seatNr, plane[seatNr-1].seatAssigned);
        if (plane[seatNr-1].seatAssigned) {
            puts("That seat is already assigned.  Choose another.");
            continue;
        }
        else
            validEntry = 1;
    }    
    plane[seatNr-1].seatID = seatNr;
    
    i = 0;
    do {
        printf("Enter the last name of the passenger: ");
        s_gets(plane[seatNr-1].paxLname, 40);
        if (strlen(plane[seatNr-1].paxLname) == 0) {
            puts("Terminating Asign Seat function.");
            plane[seatNr-1] = emptySeat;
            return;
        }
        if (isalpha(plane[seatNr-1].paxLname[0]))
            i = 1;
    } while (i == 0);

    i = 0;
    do {
        printf("Enter the first name of the passenger: ");
        s_gets(plane[seatNr-1].paxFname, 40);
        if (strlen(plane[seatNr-1].paxFname) == 0) {
            puts("Terminating Asign Seat function.");
            plane[seatNr-1] = emptySeat;
            return;
        }
        if (isalpha(plane[seatNr-1].paxFname[0]))
            i = 1;
    } while (i == 0);
    plane[seatNr-1].seatAssigned = 1;
}


//****************************************************************************
void deleteSeatAssign(struct seat * flt)
{

}


//****************************************************************************
void endProgram()       // 'f' == QUIT
{

}


//****************************************************************************
char * s_gets(char * st, int n)
{
    char * ret_val;
    int i = 0;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        while (st[i] != '\n' && st[i] != '\0')
            i++;
        if (st[i] == '\n')
            st[i] = '\0';
        else
            while (getchar() != '\n')
                continue;
    }
    return ret_val;
}


//****************************************************************************
void strngcpy(char * dest, char * src)
{

     dest[0] = '\0';
     strncat(dest, src, MAX_CITY_NAME_LEN);

}


//****************************************************************************
int backUpFile(char * dest, char * src)
{
    FILE * destPtr;
    FILE * srcPtr;
    unsigned char chunk[1024];

    int x;
    size_t i;
    size_t j;

    srcPtr = fopen(FLIGHT_LIST_FILE_NAME, "rb");
    destPtr = fopen(FLIGHT_LIST_FILE_NAME_BAK, "wb");

    if (!srcPtr)
        return (-1);
    if (!destPtr) {
        fclose(srcPtr);
        return (-1);
    }

    do {
        i = fread(chunk, 1, sizeof(chunk), srcPtr);
        if (i)
            j = fwrite(chunk, 1, i, destPtr);
        else
            j = 0;
    } while ((i > 0) && (i == j));

    fclose(destPtr);
    fclose(srcPtr);

    return (0);
}

