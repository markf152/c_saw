//------------------------------------------------------------------
// 11-transform.c -- implements function transform() that takes four
// arguments:
//           - name of source array of type double
//           - name of target array of type double
//           - an int representing the number of array elements
//           - name of a function or pointer to a function
// 
// Program to test transform() four times
//           - 2 x functions from math.h; I choose . . .
//             - double sqrt(double)
//                 (returns largest integer less <= double)
//             - double floor(double) 
//           - 2 x 'suitable' functions of my design
// -----------------------------------------------------------------

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#define MAX_ARRAY_ELEMENTS 1000


double transform (double * source, double  * target, 
               int numElements, double (* funcPtr)(double));

// myFuncA - multiplies type double input by 1.3
//           returns type double result
double myFuncA (double input); 


// myFuncB - multiplies type double input by 1.3, then adds 2.2
//           returns type double result
double myFuncB (double input);

char * s_gets(char * st, int n);

int main (void)
{

    double testSource[10] = { 16.1, 10.0, 45.6, 32.1, 3.0,
                              98.5, 64.0, 48.7, 8.4, 31.0 };
    double target[10] = { 0, 0, 0, 0, 0, 
                          0, 0, 0, 0, 0 }; 

    double (*funcPointer)(double);

    int x; // index / loop variable

    puts("\n");
    

    funcPointer = sqrt;
    transform(testSource, target, 10, funcPointer);
    for (x = 0; x < 10; x++)
        printf("Source[%2d] = %9.4f    Target[%2d] = %9.4f\n", x, testSource[x], x, target[x]);
    puts("\n");


    funcPointer = floor;
    transform(testSource, target, 10, funcPointer);
    for (x = 0; x < 10; x++)
        printf("Source[%2d] = %9.4f    Target[%2d] = %9.4f\n", x, testSource[x], x, target[x]);
    puts("\n");


    funcPointer = myFuncA;
    transform(testSource, target, 10, funcPointer);
    for (x = 0; x < 10; x++)
        printf("Source[%2d] = %9.4f    Target[%2d] = %9.4f\n", x, testSource[x], x, target[x]);
    puts("\n");


    funcPointer = myFuncB;
    transform(testSource, target, 10, funcPointer);
    for (x = 0; x < 10; x++)
        printf("Source[%2d] = %9.4f    Target[%2d] = %9.4f\n", x, testSource[x], x, target[x]);


    puts("\n");

    return (0);
}
//--------------------------------------------------------------------------



double transform (double * source, double * target, 
               int numElements, double (* funcPtr)(double))
{
    int x = 0;

    for (x = 0; x < numElements; x++) {
       target[x] = (*funcPtr)(source[x]);
    }

    return (0);
}
//--------------------------------------------------------------------------



double myFuncA (double input)
{
    return (input * 1.3);
}
//--------------------------------------------------------------------------


double myFuncB (double input)
{

    return ((input * 1.3) + 2.2);
}
//--------------------------------------------------------------------------

char * s_gets(char * st, int n)
{
    char * ret_val;
    int i = 0;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        while (st[i] != '\n' && st[i] != '\0')
            i++;
        if (st[i] == '\n')
            st[i] = '\0';
        else                             // obviously we get here *only* if st[i] == '\0'
            while (getchar() != '\n')    // these two lines clean out the input buffer
                continue;                //    since we haven't yet found a '\n'
    }
    return ret_val;
}
