﻿// 08-airTEST.c -- Seating reservation program for Colossus Airlines 
//
// <<< *** git test comment *** >>>
//
// prog exercise . . . loc 18159
// menu techniques . . .loc 9154 / text pg 324
//
// Flights are represented
//  Individual flights is represented by an array of structures, one for each seat:
//  - seat ID number
//  - marker - seat assigned or not
//  - last name of seat holder
//  - first name of seat holder
// 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>  // need for access() function to check existance of file
#include <stdbool.h>
#include <ctype.h>

#define FLIGHT_LIST_FILE_NAME "flightList.dat" // flight list file name
#define FLIGHT_LIST_FILE_NAME_BAK "flightListbak.dat" // back-up file name

#define MAX_NAME_LEN 40
#define MAX_CITY_NAME_LEN 40
#define MAX_FLIGHTS 500
#define NUMSEATS 12
#define MAX_FLIGHTS_PER_MENU_PAGE 10

struct flightInfo {
    int flightNum;
    int numSeats;
    char fromCity[MAX_CITY_NAME_LEN];
    char toCity[MAX_CITY_NAME_LEN];
    char confirm;
};

struct seat {
    int seatID;                // 1 through NUMSEATS (0 is not used)
    int seatAssigned;          // 0 = not assigned; non-zero = assigned
    int seatConfirmed;
    char paxLname[MAX_NAME_LEN];
    char paxFname[MAX_NAME_LEN];
};

//Utility Functions . . .
char * s_gets(char * st, int n);
int getfile(char * fileName, FILE ** filePtr);
void createEmptySeats(FILE ** flt, int numSeats);
void endProgram();       // 'q' == QUIT
void strngcpy(char * dest, char * src);
int backUpFile(char * dest, char * src);

//Menu Functions . . .
char mainMenu(void);
int flightListMenu(struct flightInfo *listOfFlights, int numFlights);
int editFlightMenu(struct flightInfo flight);

//Show, Add, Delete Flights . . .
int sortByFltNum(struct flightInfo * listOfFlights, int numFlights);
int sortByFromCity(struct flightInfo * listOfFlights, int numFlights);
int sortByToCity(struct flightInfo * listOfFlights, int numFlights);
int addFlight(struct flightInfo * listOfFlights, int numFlights, char * flightList);
int getExistingFlights(FILE ** flightListFile, 
                         struct flightInfo * flightList);
int deleteFlight(struct flightInfo * listOfFlights, int numFlights, 
                   char * flightList);

//Functions for individual flight work . . .
void loadFlightData(FILE ** flt, struct seat * plane, int seats);
void showNrEmptySeats(const struct seat *);
void listEmptySeats(const struct seat * flt);
void showAlphaListPax(const struct seat *, int seats);
void assignPaxSeat(struct seat *);
void deleteSeatAssign(struct seat *);


int main (void)
{
    struct seat plane1[NUMSEATS];
    struct flightInfo listOfFlights[MAX_FLIGHTS];

    FILE * flightListFile;
    FILE * flightFile;

    int seatSize = sizeof(struct seat);
    int flightInfoSize = sizeof(struct flightInfo);
    int menuChoice;
    int i; // generic loop counter
    int numFlights;  // total number of flights for airline

    puts("\n");

    getfile(FLIGHT_LIST_FILE_NAME, &flightListFile);
    numFlights = 0;
    while (numFlights < MAX_FLIGHTS && fread(&listOfFlights[numFlights], 
           flightInfoSize, 1, flightListFile) == 1) {
        numFlights++;
    }    
    fclose(flightListFile);

    flightListMenu(listOfFlights, numFlights);

    puts("\n");

}

//****************************************************************************
int getfile(char * fileName, FILE ** filePtr)
{
    int i;

    if ((*filePtr = fopen(fileName, "r+b")) == NULL) {
        printf("File %s can't be opened for update, will check existence and try to create\n", fileName);
        if (access(fileName, F_OK) == -1) {
            printf("File %s fails 'access()' existence check, attempting to create. \n", fileName);
            if ((*filePtr = fopen(fileName, "w+b")) == NULL) {
                fprintf(stderr, "Can't open or create '%s' file. Program ending.\n\n", 
                      fileName);
                return 2;
            }
            else {
                printf("File %s created\n\n",fileName);
                return 1;  // 0 = new file created and opened
            }
        }
        else {
            printf("File %s exists, but can't be opened for update.  Program ending.\n",fileName);
            exit(EXIT_FAILURE);
        }
    }
    else
//        printf("File %s is opened for update.\n", fileName);
        return 0;  // 0 = file exists and was opened successfully

//    printf("flt = %p\n", *flt);
//    puts("ok rewind . . .");
}


//****************************************************************************
void createEmptySeats(FILE ** flt, int numSeats)
{
    int seatSize = sizeof(struct seat);
    struct seat emptySeat = {0, 0, 0, 0, 'N'};
    int i;  // loop variable
    
    rewind(*flt);

    for (i = 0; i < numSeats; i++) {  // create each empty seat
        emptySeat.seatID = i;

        if (((i + 1) % 2) || ((i + 1) == 8)) // temp for testing
            emptySeat.seatAssigned = 1; // temp for testing
        else                            // temp for testing
            emptySeat.seatAssigned = 0; // temp for testing

//        printf("Seat %2d: %d\n", (i + 1), emptySeat.seatAssigned);

        fwrite(&emptySeat, seatSize, 1, *flt);
    }
}

//****************************************************************************
void loadFlightData(FILE ** flt, struct seat * plane, int seats)
{
    int i;
    int x;   // temp for testing
    int seatSize = sizeof(struct seat);
//    printf("plane = %p\n", plane);

    rewind(*flt);
//    printf("flt = %p\n", *flt);

    for (i = 0; i < seats; i++) {
//        printf("i = %2d  ", i);
//        x = fread(plane++, seatSize, 1, *flt);
//        printf("x = %2d\n", x);
        if (fread(plane++, seatSize, 1, *flt) != 1) {
            puts("Error . . . couldn't read in all seats for flight.");
            puts("Contact tech support.  Program ending.");
            puts("Function = loadFlightData");
            exit(1);            
        }

/* This entire section for testing . . . 
        plane--;
        printf("file location: %ld\n", ftell(*flt));
        printf("seatID: %d\n", plane->seatID);
        printf("seatAssigned: %d\n", plane->seatAssigned);
        printf("paxLname: %s\n", plane->paxLname);
        printf("paxFname: %s\n", plane->paxFname);
        plane++;
*/

    }
//    puts("read in all seats");
}


//****************************************************************************
char mainMenu(void)
{
    puts("Select a function (enter its letter)...");
    puts("a) Pick a flight");
    puts("b) Create a flight");
    puts("c) Delete a flight");
    puts("q) Quit");
}


//****************************************************************************
int flightListMenu(struct flightInfo * listOfFlights, int numFlights)
{
    int flightInfoSize = sizeof(struct flightInfo);
    int i = 0; // general index variable
    int j = 0; // general index variable
    int fltNum; // temp flight number variable
    int page = 0; // marker to track what part of list of flights is displayed
    char menuChoice[5] = "xxxx";
    char * junkPtr;  // pointer needed for strtol() - not used otherwise
    char quit[3] = "x";
    bool validMenuChoice = false;

    while (quit[0] != 'Y' && quit[0] != 'y') {
        puts("Flt Nr   From                To");
        puts("--------------------------------------------------------------");
        for (i = page; (i < numFlights) && (i < (page + 10)); i++) {
            printf("%d     %s",
                listOfFlights[i].flightNum, 
                listOfFlights[i].fromCity);
            for (j = 0; j < (20 - strlen(listOfFlights[i].fromCity)); j++)
                printf(" ");
            printf("%s\n",listOfFlights[i].toCity);
        }
    
        if (i == 0) // no flights stored in flightList file
            printf("<<< No flights in the current data file >>>\n");
        printf("\n");
    
        puts("(L)Next screen / (P)revious screen");
        puts("Sort by Flight (N)umber / (F)rom City / (T)o City");
        puts("Enter flight number / (A)dd flight / (D)elete flight / (Q)uit");
    
        s_gets(menuChoice, 5);
        if (isalpha(menuChoice[0])) {
            if (strchr("LPNFTADQlpnftadq", menuChoice[0])) {
                validMenuChoice = true;
                switch (menuChoice[0]) {
                    case 'L' :
                    case 'l' :
                        if ((page + 10) < numFlights)
                            page += 10;
                        break;
                        case 'P' :
                        case 'p' :
                        page -= 10;
                        if (page < 0)
                            page = 0;
                        break;
                    case 'N' :
                    case 'n' :
                        // sort by flight number
                        sortByFltNum(listOfFlights, numFlights);
                        break; 
                    case 'F' :
                    case 'f' :
                        // sort by From city
                        sortByFromCity(listOfFlights, numFlights);
                        break;
                    case 'T' :
                    case 't' :
                        // sort by To city
                        sortByToCity(listOfFlights, numFlights);
                        break;
                    case 'A' :
                    case 'a' :
                        // Add a flight
                        i = addFlight(listOfFlights, numFlights, 
                                      FLIGHT_LIST_FILE_NAME);
                        if (i > numFlights)
                            numFlights = i;
                        break;
                    case 'D' :
                    case 'd' :
                        // Delete a flight
                        i = deleteFlight(listOfFlights, numFlights, 
                                         FLIGHT_LIST_FILE_NAME);
                        if (i < numFlights)
                            numFlights = i;
                        break;
                    case 'Q' :
                    case 'q' :
                        // Quit
                        puts("Are you sure you want shut down the program?");
                        puts("Enter 'Y' or 'y' to confirm or any other key to continue");
                        s_gets(quit, 2);  // looking for quit[0] == 'Y' or 'y' to quit
                        break;
                }
            }
        }
        else  {
            fltNum = strtol(menuChoice, &junkPtr, 10); 
            for (i = 0; i < numFlights; i++)
                if (listOfFlights[i].flightNum == fltNum) {
                    printf("Valid flight number %d\n\n", fltNum);
                    validMenuChoice = true;
                    editFlightMenu(listOfFlights[i]); 
                }
            if (!validMenuChoice)       
                printf("Not a valid menu choice or flight number.  Try again.\n\n");
        }

    } 
}

//****************************************************************************
int sortByFltNum(struct flightInfo * listOfFlights, int numFlights)
{
    struct flightInfo tempFltInfo;
    int i;  // loop counter
    int j;  // loop counter

    for (i = 0; i < (numFlights - 1); i++) {
        for (j = (i + 1); j < numFlights; j++) {
            if (listOfFlights[j].flightNum < listOfFlights[i].flightNum) {
                tempFltInfo = listOfFlights[i];
                listOfFlights[i] = listOfFlights[j];
                listOfFlights[j] = tempFltInfo;
            }
        }
    }
}


//****************************************************************************
int sortByFromCity(struct flightInfo * listOfFlights, int numFlights)
{
    struct flightInfo tempFltInfo;
    int i;  // loop counter
    int j;  // loop counter

    for (i = 0; i < (numFlights - 1); i++) {
        for (j = (i + 1); j < numFlights; j++) {
            if (strcmp(listOfFlights[i].fromCity, listOfFlights[j].fromCity) > 0) {
                tempFltInfo = listOfFlights[i];
                listOfFlights[i] = listOfFlights[j];
                listOfFlights[j] = tempFltInfo;
            }
        }
    }
}


//****************************************************************************
int sortByToCity(struct flightInfo * listOfFlights, int numFlights)
{
    struct flightInfo tempFltInfo;
    int i;  // loop counter
    int j;  // loop counter

    for (i = 0; i < (numFlights - 1); i++) {
        for (j = (i + 1); j < numFlights; j++) {
            if (strcmp(listOfFlights[i].toCity, listOfFlights[j].toCity) > 0) {
                tempFltInfo = listOfFlights[i];
                listOfFlights[i] = listOfFlights[j];
                listOfFlights[j] = tempFltInfo;
            }
        }
    }
}


//****************************************************************************
int addFlight(struct flightInfo * listOfFlights, int numFlights, char * flightList)
{
    struct flightInfo newFlight;
    struct flightInfo tempFlight; // storage location for sorting algorithm
    int fltNum = 0;
    int seats = 0;
    char fmCity[MAX_CITY_NAME_LEN];
    char newFlightFileName[16];
    char toCity[MAX_CITY_NAME_LEN];
    int i = 0;    // loop counter
    int j = 0;    // loop counter
    char string[50];
    char * temp;  // needed for strtol() -- not used otherwise
    FILE * tempFlightFile;  // for creating the new individual flight file
    FILE * flightListFilePtr;  // for opening the flight list file

    printf("Create a new flight (hit only <return> on any line to abort and return to previous menu)\n\n");

    do {
        printf("Enter the new flight number (1 - 9999) . . . ");
        s_gets(string, 5);
        if (strlen(string) == 0) {
            puts("Terminating Add Flight function.");
            return 0;
        }
        fltNum = (int)strtol(string, &temp, 10);
        if (fltNum > 0 && fltNum < 10000) {  // check for valid flt nr . . .
            i = 1;
            for (j = 0; j < numFlights; j++) {  // and chk if it already exists
                if (listOfFlights[j].flightNum == fltNum) {
                    printf("flight %d already exists.\n", fltNum);
                    i = 0;
                }
            }
        }
        printf("\n");
    } while (i == 0);

    // this section builds the flight file name using the flight number
    // it pads the file name with zeros to aid in trouble-shooting when
    // listing files 

//    printf("The current HOME is %s \n\n", getenv("HOME"));

    newFlightFileName[0] = '\0';
    switch ((int)strlen(string)) {
        case 1 :
            strncat(newFlightFileName, "08-flights/000", 14);
            strncat(newFlightFileName, string, 1);
            printf("newFlightFileName = %s\n\n", newFlightFileName);
            break;
        case 2 :
            strncat(newFlightFileName, "08-flights/00", 13);
            strncat(newFlightFileName, string, 2);
            printf("newFlightFileName = %s\n\n", newFlightFileName);
            break;
        case 3 :
            strncat(newFlightFileName, "08-flights/0", 12);
            strncat(newFlightFileName, string, 3);
            printf("newFlightFileName = %s\n\n", newFlightFileName);
            break;
        case 4 :
            strncat(newFlightFileName, "08-flights/", 11);
            strncat(newFlightFileName, string, 4);
            printf("newFlightFileName = %s\n\n", newFlightFileName);
            break;
    }
    
    i = 0;
    do {
        printf("Enter the number of seats on flight %d . . . ", fltNum);
        s_gets(string, 5);
        if (strlen(string) == 0) {
            puts("Terminating Add Flight function.");
            return 0;
        }
        seats = (int)strtol(string, &temp, 10);
        if (seats >= 1 && seats < 500)
            i = 1;
        printf("\n");
    } while (i == 0); 

    i = 0;
    do {
        printf("Enter the name of the departure city for flight %d . . . ", fltNum);
        s_gets(fmCity, 49);
        if (strlen(fmCity) == 0) {
            puts("Terminating Add Flight function.");
            return 0;
        }
        if (strlen(fmCity) > 0)
            i = 1;
    } while (i == 0);

    i = 0;
    do {
        printf("Enter the name of the destination city for flight %d . . . ", fltNum);
        s_gets(toCity, 49);
        if (strlen(toCity) == 0) {
            puts("Terminating Add Flight function.");
            return 0;
        }
        if (strlen(toCity) > 0)
            i = 1;
    } while (i == 0);

    listOfFlights[numFlights].flightNum = fltNum;
    listOfFlights[numFlights].numSeats = seats;
    strngcpy(listOfFlights[numFlights].fromCity, fmCity);
    strngcpy(listOfFlights[numFlights].toCity, toCity);
    numFlights++;

    backUpFile(FLIGHT_LIST_FILE_NAME, FLIGHT_LIST_FILE_NAME_BAK);

    flightListFilePtr = fopen(FLIGHT_LIST_FILE_NAME, "wb");
    if (!flightListFilePtr) {
        fputs("Error opening flight list file for writing.  Ending Program", stderr);
        exit(EXIT_FAILURE);
    }

    j = fwrite(listOfFlights, sizeof(struct flightInfo), numFlights, flightListFilePtr);
    if (j != numFlights) {  //test to make sure all flights were written
            fputs("Error - not all flights written to file\n", stderr);
            exit(EXIT_FAILURE);
    }

    fclose(flightListFilePtr);

//    fseek(*flightList, 0, SEEK_END);
//    fwrite(&listOfFlights[numFlights], sizeof(struct flightInfo), 1, *flightList);


    printf("\n\n");
    printf("numFlights = %d\n\n", numFlights);
    printf("For new flight number %d . . .\n", listOfFlights[numFlights].flightNum);
    printf("The number of seats is %d\n", listOfFlights[numFlights].numSeats); 
    printf("The departure city is %s\n", listOfFlights[numFlights].fromCity);
    printf("The destination city is %s\n\n", listOfFlights[numFlights].toCity);

    i = getfile(newFlightFileName, &tempFlightFile);
    if (i != 1)
        switch (i) {
            case 0 :
                puts("A flight file for that flight number already exists\n");
                puts("Delete that file and try to create the new flight again.\n");
                puts("Terminating Add Flight function.\n");
                return 0;
            case 2 :
                puts("The new flight file can't be created\n");
                puts("There is a system problem.  Program ending.\n");
                exit(EXIT_FAILURE);
            default :
                puts("An unspecified error occured.  Program ending.\n");
                exit(EXIT_FAILURE);
        }
    createEmptySeats(&tempFlightFile, seats);
    fclose(tempFlightFile);
    return (numFlights);
}


//****************************************************************************
int deleteFlight(struct flightInfo * listOfFlights, int numFlights, char * flightList)
{
    char string[50];
    int fltNum = 10000;
    int i = 0; // index variable
    int j = 0; // index variable
    int done = 0;
    char * temp;  // needed for strtol() -- not used otherwise
    FILE * flightListFilePtr;  // for opening the flight list file
    
    while (!done) {
        printf("Enter the number of the flight to delete <return> to abort: ");
        s_gets(string, 5);
        if (strlen(string) == 0) {
            puts("Aborting delete flight");  
            return(numFlights);
        }
        fltNum = (int)strtol(string, &temp, 10);
        printf("fltNum = %d\n", fltNum);
        if (fltNum >= 0 && fltNum < 10000) {  // check for valid flt nr . . .
            // . . . and step through until we find it
            for (i = 0, j = 0; i < numFlights; i++, j++) {
                if (listOfFlights[i].flightNum == fltNum) {
                    printf("You have selected this flight to delete:\n");
                    printf("Flight Number: %d\n", listOfFlights[i].flightNum);
                    printf("From: %s\n", listOfFlights[i].fromCity);
                    printf("To: %s\n", listOfFlights[i].toCity);
                    printf("Seats: %d\n", listOfFlights[i].numSeats);
                    done = 1;
                    j++;
                }
                printf("i = %d   j = %d\n", i, j);
                if (i != j) {
                    listOfFlights[i].flightNum = listOfFlights[j].flightNum;
                    strngcpy(listOfFlights[i].fromCity, listOfFlights[j].fromCity);
                    strngcpy(listOfFlights[i].toCity, listOfFlights[j].toCity);
                    listOfFlights[i].numSeats = listOfFlights[j].numSeats;
                }
            }
        }
        
        if (done)
            numFlights--;
        else
            puts("Flight number entered is not valid.");
    }

    backUpFile(FLIGHT_LIST_FILE_NAME, FLIGHT_LIST_FILE_NAME_BAK);
    flightListFilePtr = fopen(FLIGHT_LIST_FILE_NAME, "wb");
    if (!flightListFilePtr) {
        fputs("Error opening flight list file for writing.  Terminating program", stderr);
        exit(EXIT_FAILURE);
    }
    i = fwrite(listOfFlights, sizeof(struct flightInfo), numFlights, 
               flightListFilePtr);
    if (i != numFlights)
        fputs("Error - not all flights written to file\n", stderr);
    return (numFlights);
}


//****************************************************************************
int editFlightMenu(struct flightInfo flight)
{
    int sort = 0; // 0 = by flt num
                  // 1 = by passenger last name
                  // 2 = by empty / seat nr, then reserved / seat nr

    FILE * flightFile;  // flight file
   
    // to build file name, convert fltNum to string
    //    using sprintf() 
    // build rest of file name (pad w/ 0's as needed)

    printf("Flight Number is %d\n\n", flight.flightNum);

    printf("\n\nWorking Flight Nr %d\n\n\n", flight.flightNum);
    puts("Choose a function (enter its letter)...");
    puts("a) Show number of empty seats");
    puts("b) Toggle list of empty seats (seat nr, alpha by pax, empty");
    puts("c) Show alphabetical list of seats");
    puts("d) Assign a customer to a seat assignment");
    puts("e) Confirm a customer's seat assignment");
    puts("f) Delete a seat assignment");
    puts("r) Return to Main Flights Menu\n");
}


//****************************************************************************
int getExistingFlights(FILE ** flightListFile, 
                         struct flightInfo * flightList)
// returns number of existing flights (i)
{
    int i = 0; // increments to count number of flights listed in 
               // file pointed to by flightList

    rewind(*flightListFile);
    while (fread(&flightList[i], (sizeof(struct flightInfo)), 
           1, *flightListFile) == 1) {
        i++;
    }
    return i;
}


//****************************************************************************
void showNrEmptySeats(const struct seat * plane)
{
    int i;
    int filled = 0;

    for (i = 0; i < NUMSEATS; i++) {
//        printf("plane[%d].seatID = %d\n", i, plane->seatID);
//        printf("plane[%d].seatAssigned = %d\n", i, plane->seatAssigned);
        if (plane++->seatAssigned)
            filled++;
    }
    printf("%d seats are currently filled.\n\n", filled);
}


//****************************************************************************
void listEmptySeats(const struct seat * plane)
{
    int i;

    printf("The following seats are empty:\n");

    for (i = 0; i < NUMSEATS; i++)  {
        if (!(plane->seatAssigned)) 
            printf("%d, ", (plane->seatID + 1));
        plane++;
    } 
    
    printf("\n\n");
}


//****************************************************************************
void showAlphaListPax(const struct seat * plane, int numSeats)
{
    int i;
    int j;
    int temp; // temp storage during sort
    int *  sortedSeats;
    sortedSeats = (int *)malloc(numSeats * sizeof(int)); 
    
    for (i = 0; i < numSeats; i++) 
        sortedSeats[i] = i;

    for (i = 1; i < numSeats; i++) {
        for (j = i; j > 0; j--)
            if (strcmp(plane[j].paxLname, plane[j-1].paxLname) > 0) {
                temp = sortedSeats[j-1];
                sortedSeats[j-1] = sortedSeats[j];
                sortedSeats[j] = temp;
            }
    }
}


//****************************************************************************
void assignPaxSeat(struct seat * flt)
{

}


//****************************************************************************
void deleteSeatAssign(struct seat * flt)
{

}


//****************************************************************************
void endProgram()       // 'f' == QUIT
{

}


//****************************************************************************
char * s_gets(char * st, int n)
{
    char * ret_val;
    int i = 0;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        while (st[i] != '\n' && st[i] != '\0')
            i++;
        if (st[i] == '\n')
            st[i] = '\0';
        else
            while (getchar() != '\n')
                continue;
    }
    return ret_val;
}


//****************************************************************************
void strngcpy(char * dest, char * src)
{
    
     dest[0] = '\0';
     strncat(dest, src, MAX_CITY_NAME_LEN);

}


//****************************************************************************
int backUpFile(char * dest, char * src)
{
    FILE * destPtr;
    FILE * srcPtr;
    unsigned char chunk[1024];

    int x;
    size_t i;
    size_t j;

    srcPtr = fopen(FLIGHT_LIST_FILE_NAME, "rb");
    destPtr = fopen(FLIGHT_LIST_FILE_NAME_BAK, "wb");

    if (!srcPtr)
        return (-1);
    if (!destPtr) {
        fclose(srcPtr);
        return (-1);
    }

    do {
        i = fread(chunk, 1, sizeof(chunk), srcPtr);
        if (i)
            j = fwrite(chunk, 1, i, destPtr);
        else
            j = 0;
    } while ((i > 0) && (i == j));

    fclose(destPtr);
    fclose(srcPtr);    
    
    return (0);
}

