// 10-funcPtrArray.c -- uses an array of pointers to functions

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define LEN 81
#define NUM_MENU_ITEMS 4

char * s_gets(char * st, int n);

char showmenu(void);
void eatline(void);   // read through end of line
void show(void (* fp)(char *), char * str);
void toUpper(char *);
void toLower(char *);
void transpose(char *);
void dummy(char *);

int main(void)
{
    char line[LEN];
    char copy[LEN];
    char choice;
    int i;  // loop / counter variable
    void (*execFuncPtr)(char *);

    struct menuPtr {
        char itemName;
        void (*pfun)(char *);
    };

    struct menuPtr menuArray[NUM_MENU_ITEMS] = {
        {'u', toUpper},
        {'l', toLower},
        {'t', transpose},
        {'o', dummy}
    };

    puts("\n");

    puts("Enter a string (empty line to quit):");
    while (s_gets(line, LEN) != NULL && line[0] != '\0') {
        while ((choice = showmenu()) != 'n') {
            for (i = 0; i < NUM_MENU_ITEMS; i++) {
                if (choice == menuArray[i].itemName) {
                    execFuncPtr  = *menuArray[i].pfun;
                }
            }
            strcpy(copy, line);  // make copy for show()
            show(execFuncPtr, copy);    // use selected function
        }
        puts("Enter a string (empty line to quit):");
    }
    puts("Bye!\n");

    return 0;
}

char showmenu(void)
{
    char ans;
    puts("");
    puts("Enter menu choice:");
    puts("u) uppercase         l) lowercase");
    puts("t) transposed case   o) original case");
    puts("n) next string");
    ans = getchar();
    ans = tolower(ans);
    eatline();
    while (strchr("ulton", ans) == NULL) {
        puts("Please enter: u, l, t, o, or n . . . ");
        ans = tolower(getchar());
        eatline();
    }

    return ans;
}

void eatline(void)
{
    while (getchar() != '\n')
        continue;
}

void toUpper(char * str)
{
    while (*str) {
        *str = toupper(*str);
        str++;
    }
}
 
void toLower(char * str)
{
    while(*str) {
        *str = tolower(*str);
        str++;
    }
}

void transpose(char * str)
{
    while(*str) {
        if (islower(*str))
            *str = toupper(*str);
        else if (isupper(*str))
            *str = tolower(*str);
        str++;
    }
}

void dummy(char * str)
{
    // leaves string unchanged
}

void show(void (*fp)(char *), char * str)
{
    (*fp)(str);
    puts(str);
}

char * s_gets(char * st, int n)
{
    char * ret_val;
    int i = 0;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        while (st[i] != '\n' && st[i] != '\0')
            i++;
        if (st[i] == '\n')
            st[i] = '\0';
        else
            while (getchar() != '\n')
                continue;
    }
    return ret_val;
}

