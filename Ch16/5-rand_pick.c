/* 5-rand_pick.c -- program to test function that takes as arguments */
/* (1) name of an array of type int elements, (2) size of the array */
/* and (3) value representing the number of picks */
/* --> function makes the requested picks at random and prints them */
/* - no array element to be picked more than once */
/* - use time() to initialize srand() */
/* problem location in text = 20683 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>  // for rand()

#define ARRAY_SIZE 100
/* caution - do not make ARRAY_SIZE larger than 32767 w/out checking RAND_MAX */
/* RAND_MAX will be at least 32767 */

int rand_int_array_pick(int * array_of_int, int array_size, int num_picks);

int main (void)
{
    int array[ARRAY_SIZE];
    int array_size = ARRAY_SIZE;
    int num_picks = 1;
    int i = 0;  // index variable for loops

    puts("\n");

    for (i = 0; i < ARRAY_SIZE; i++)
        array[i] = i;  // initialize array

    printf("RAND_MAX = %d\n\n", RAND_MAX);
    printf("The array has %d elements. Enter the number of picks you want: ", ARRAY_SIZE);
    scanf("%d", &num_picks);
    printf("\n");
    if (num_picks > ARRAY_SIZE) {
        num_picks = ARRAY_SIZE;
        printf("Number of picks requested greater than array size (%d).  Picking %d.\n\n", 
                ARRAY_SIZE, ARRAY_SIZE);
    }

    rand_int_array_pick(array, ARRAY_SIZE, num_picks);

    puts("\n");
    
    return (0);
}

int rand_int_array_pick(int * array_of_int, int array_size, int num_picks)
{

    int picked_elements[ARRAY_SIZE];
    int i;  // index variable for loops
    int pick_ok = 0;
    int pick = 0;
    int num_places = 0;  // number of places occupied by number of largerst element in the array
    int max = 0;  // used to store maximum value in the array
    int m = 0;    // used by algorithm that determines number of places occupied by number

    // initialize picked elements array . . .
    // . . . and find max value element in the array
    for (i = 0; i < array_size; i++) {
        picked_elements[i] = 0;
        if (array_of_int[i] > max)
            max = array_of_int[i];
    }

    // algorithm determines number of places occupied by max value element from array    
    i = 0;
    m = max;
    while (m != 0) {
        m /= 10;
        ++num_places;
    }

    for (i = 0; i < num_picks; i++) {
        pick_ok = 0;
        do {
            pick = rand() % array_size;
            if (!picked_elements[pick]) {
                picked_elements[pick] = 1;
                pick_ok = 1;
            }
        } while (!pick_ok);
        printf("%*d", num_places, pick);
        if ((((i+1) % 10) == 0) || (i == (num_picks-1))) 
            printf("\n");
        else
            printf(", ");
    }

}

