// generic.c -- defining generic macros

#include <stdio.h>
#include <math.h>

#define RAD_TO_DEG (180/(4 * atanl(1)))

// generic square root function
#define SQRT(X) _Generic((X),\
    long double: sqrtl, \
    default: sqrt, \
    float: sqrtf)(X)

// generic sine function, angle in degrees
// SQRT(X) is more 'elegant' in that the _Generic
//   part returns an address / function pointer which
//   is followed by the argument, X.  That calls the
//   pointed-to function with argument X.
#define SIN(X) _Generic((X),\
    long double: sinl((X)/RAD_TO_DEG),\
    default:     sin((X)/RAD_TO_DEG),\
    float:       sinf((X)/RAD_TO_DEG)\
)

int main(void)
{
    float x = 45.0f;
    double xx = 45.0;
    long double xxx = 45.0L;

    long double y = SQRT(x);
    long double yy = SQRT(xx);
    long double yyy = SQRT(xxx);

    puts("\n");

    printf("%.17Lf\n", y);   // matches float
    printf("%.17Lf\n", yy);  // matches defualt
    printf("%.17Lf\n", yyy); // matches long double

    int i = 45;

    yy = SQRT(i);
    printf("%.17Lf\n", yy);
    yyy = SIN (xxx);
    printf("%.17Lf\n", yyy);

    puts("\n");

    return(0);
}

