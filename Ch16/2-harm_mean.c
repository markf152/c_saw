/* harm_mean.c -- use a macro to implment a harmonic mean function */
/* harmonic mean of 2 numbers: take inverse of each, average, take */
/* inverse of the result */

#include <stdio.h>

#define HARM_MEAN(X,Y) 1.0 / (((1.0/X) + (1.0/Y)) / 2.0)

int main(void)
{

    puts("\n");

    puts("Harmonic Mean of 4.0 and 9.0:");
    printf("%f\n", HARM_MEAN(4.0,9.0));

    puts("\n");

    return (0);
}

