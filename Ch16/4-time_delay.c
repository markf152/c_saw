/* time_delay.c -- tests a function that runs a loop to create a desired */
/* time delay before returning */

#include <stdio.h>
#include <time.h>

int execute_delay (double delay_time);

int main(void)
{
    double rqst_delay;

    time_t rawtime;
    struct tm * timeinfo;

    puts("\n");

    puts("Enter the desired delay in seconds (float value)");
    scanf("%lf", &rqst_delay);

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    printf("Time: %s\n", asctime(timeinfo));

    execute_delay(rqst_delay);

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    printf("Time: %s\n", asctime(timeinfo));

    puts("\n");

    return (0);
}

int execute_delay (double delay_time)
{
    int done = 0;
    double start_time = (double)(clock());
    do  {
        if (((clock() - start_time) / CLOCKS_PER_SEC) >= delay_time)
            done = 1;
    } while (!done);

}

