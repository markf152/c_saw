/* variadic.c -- variadic macros */

#include <stdio.h>
#include <math.h>

#define PR(X, ...) printf("Message " #X ": "__VA_ARGS__)

int main(void)
{
    double x = 48;
    double y;

    const char * fmt = "X is %d.\n";

    puts("\n");

    printf("%s", fmt);

    y = sqrt(x);
    PR(1, "x = %g\n", x);
    PR(2, "x=%.2f, y = %.4f\n", x, y);

    puts("\n");

    return 0;
}

