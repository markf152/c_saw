// names_st.c -- define names_st functions

#include <stdio.h>
#include "names_st.h"  // include the header file

// function definitions
void get_names(names * pn)
{
    printf("Please enter your first name: ");
    s_gets(pn->first, SLEN);

    printf("Please enter your last name: ");
    s_gets(pn->last, SLEN);
}

void show_names(const names * pn)
{
    printf("%s %s", pn->first, pn->last);
}


char * s_gets(char * st, int n)
{
    char * ret_val;
    int i = 0;

    ret_val = fgets(st, n, stdin);
    if (ret_val) {
        while (st[i] != '\n' && st[i] != '\0')
            i++;
        if (st[i] == '\n')
            st[i] = '\0';
        else                             // obviously we get here *only* if st[i] == '\0'
            while (getchar() != '\n')    // these two lines clean out the input buffer
                continue;                //    since we haven't yet found a '\n'
    }
    return ret_val;
}

