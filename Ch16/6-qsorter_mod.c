/* 6-qsorter_mod.c -- using qsort to sort a array of structs
   modification of text listing 16.17 (qsorter.c) . . .
   (problem loc 20684)
   - uses array of struct instead of array of double
       (text loc 20357)
       struct names {
           char first[40];
           char last[40];
       };
       struct names staff[100];
       qsort(staff, 100, sizeof(struct names), comp);
 - uses fewer elements with the array explicitly
    initialized to a suitable selection of names */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM 10

struct names {
    char first[40];
    char last[40];
};

void showarray(struct names name_lst[], int n);
int mycomp(const void * p1, const void * p2);

int main(void)
{
    struct names name_list[10] = {
        {"Melvin", "Jones"},
        {"Wilbert", "Smith"},
        {"Friedrich", "Hoffmann"},
        {"Adam", "Schnitzelfinker"},
        {"Gertrude", "Wilson"},
        {"Megan", "Freud"},
        {"Becky", "Duffeldorf"},
        {"Wilehmina", "Jenkins"},
        {"Bruce", "Hooper"},
        {"Zeke", "Lopez"}
     };

    puts("\n");

    puts("Random list:");
    puts("-----------");
    showarray(name_list, NUM);
    printf("\n");

    qsort(name_list, NUM, sizeof(struct names), mycomp);

    puts("\nSorted list:");
    puts("-------------");
    showarray(name_list, NUM);

    puts("\n");

    return (0);
}

void showarray (struct names name_lst[], int n)
{
    int i;
    
    for (i = 0; i < n; i++)
        printf("%s %s\n", name_lst[i].first, name_lst[i].last);
}

/* sort by increasing value */
int mycomp (const void * p1, const void * p2)
{
    /* need to use pointers to struct names to access values */
    const struct names * a1 = (const struct names *) p1;
    const struct names * a2 = (const struct names *) p2;
    int result;

    result = strcmp(a1->last, a2->last);

    if (result != 0)
        return result;
    else
        return (strcmp(a1->first, a2->first));
}

