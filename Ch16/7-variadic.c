/* 7-variadic.c -- demonstrates 'variadic' function capability of C */
/* this is discussed in the text at loc 20505 */

/* problem loc 20701 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

void show_array(const double ar[], int n);
double * new_d_array(int n, ...);

int main()
{
    double * p1;
    double * p2;

    p1 = new_d_array(5, 1.2, 2.3, 3.4, 4.5, 5.6);
    p2 = new_d_array(4, 100.0, 20.00, 8.08, -1890.0);

    puts("\n");

    show_array(p1, 5);
    show_array(p2, 4);
    free(p1);
    free(p2);

    puts("\n");

    return (0);
}

double * new_d_array(int n, ...)
{
    double * ret_addr;
    int i;
    va_list da_ptr;

    va_start(da_ptr, n);
    ret_addr = (double *)(malloc(n * sizeof(double)));    
    for (i = 0; i < n; i++) 
        ret_addr[i] = va_arg(da_ptr, double);
    va_end(da_ptr);
    
    return(ret_addr);
}

void show_array(const double ar[], int n)
{
    int i;

    for (i = 0; i < n; i++) {
        printf("%.2f", ar[i]);
        if (i != (n-1))
            printf(", ");
        else
            printf("\n");
    }
    
    return; 
}

