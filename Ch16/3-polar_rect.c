/* polar_rect.c -- takes polar coordinates as input and returns */
/* a struct containing the corresponding rectangular coordinates */

/* Problem Text Location 20664 */

/* Will try to use 'anonymous union' (text loc 17464) just to use it */

#include <stdio.h>
#include <math.h>

#define PI 3.14159265

struct polar_coords {
    double radius;
    double  angle;      // expressed in radians
};

struct rect_coords {
    double x;
    double y;
};

struct coordinates {
    char coord_sys;    // p = polar, r = rectangular
    union {
        struct polar_coords polar;
        struct rect_coords rect;
    };
};

int polar_to_rect(struct coordinates * in_coords, struct coordinates * out_coords);

int main(void)
{

    struct coordinates input_coords;
    struct coordinates output_coords;

    double angle_deg = 0.0;   // used to take input in degrees

    input_coords.coord_sys = 'p';
    output_coords.coord_sys = 'r';

    puts("\n");

    puts("Enter polar coordinates, radius/magnitude, then angle (in degrees)");
    scanf("%lf %lf", &input_coords.polar.radius, &angle_deg);

    input_coords.polar.angle = angle_deg * (PI / 180);
    
    polar_to_rect(&input_coords, &output_coords);

    printf("Polar: Radius: %.2f  Angle: %.2f\n\n", input_coords.polar.radius,
                                          input_coords.polar.angle);

    puts("is equivalent to . . .");
    printf("Rectangular: X: %.2f  Y: %.2f", output_coords.rect.x,
                                            output_coords.rect.y);

    puts("\n");

    return (0);

}

int polar_to_rect(struct coordinates * in_coords, struct coordinates * out_coords)
{
    out_coords->rect.x = in_coords->polar.radius * cos(in_coords->polar.angle);

    out_coords->rect.y = in_coords->polar.radius * sin(in_coords->polar.angle);
}

